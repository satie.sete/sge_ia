# Build test case
This folder is used to create a test case. It does not use any of the rt_congestion_control code.
It is composed of two folders :
- create : With all the code
- use : With the data used for testing (the rest of the data is stored locally -- not on Git.)

The execution of this code results in the generation of 2 importants files :
- data/processed/file_number/number_of_agent : contains the time series
- data/processed/file_number/net : contains the network and all relevant info on the agents

## Create folder

### Data processing
It processes the raw data from UMASS Smart* project and Caltech EV data.
- The data is transformed into time series
- Bugs like missing data or aberant values are remooved

### Network creation
A random network is created.
It has the same carateristic as a real distribution network :
- Resistive lines
- Tree (aka radial) stucture for the low voltage part (400V between phases)
- Circular for the medium voltage part (20kV)

### Agents
Agent are a agregtion of multiple simple actors :
- PV panels
- EV charging station
- Flats

These agents have a fixed (randomly sampled) flexibility and are randomly placed on the network.

### Saved data
- Neworks, agents caracteristics and load/gen caracteristics are stored in csv files (network can also be exported to Matlab).
- Time serie are separated in 365 files (one for each simulated day) so that it can be imported easily in the simulation without craching the RAM.

## Use folder

- Contains only 10 days of simulation
- Used for testing