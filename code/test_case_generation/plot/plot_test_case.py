# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 17:30:34 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Plot the test case created in 4 different configurations :
    - Network structure : placement of agents
    - Power flow with only load
    - Power flow whith only gen
    - Power flow with both
"""

# Reproducibility
from os.path import join
import pandapower as pp
import numpy as np
from pandapower.plotting.plotly import simple_plotly, pf_res_plotly
from PIL import Image
import os
# Custom import
import code_these.src._import as imp
import code_these.src._modify_net as modif_net


def _display_load(net, fig):
    # Display load of each bus
    for k in net.load.index:
        bus, typ = net.load.loc[k][["bus", "type_agent"]]
        if typ == "apt":
            path = join(os.path.dirname(__file__),"../../","figures/plotly/apt.png")
            dx = - 0.22
            dy = 0.18
        elif typ == "ev":
            path = join(os.path.dirname(__file__),"../../","figures/plotly/ev.png")
            dx = 0
            dy = 0.18
        x, y, _ = net.bus_geodata.loc[bus]
        x += dx
        y += dy
        pyLogo = Image.open(path)
        fig.add_layout_image(dict(source=pyLogo,
                                  x=x, y=y,
                                  sizex=.2, sizey=.2,
                                  xref="x", yref="y",
                                  xanchor="center" , yanchor ="bottom"
                                  ))
    return fig

def _display_gen(net, fig):
    # Display generator on each bus
    for k in net.gen.index:
        bus, typ = net.gen.loc[k][["bus", "type_agent"]]
        path = join(os.path.dirname(__file__),"../../","figures/plotly/pv.png")
        dx = 0.22
        dy = 0.18
        x, y, _ = net.bus_geodata.loc[bus]
        x += dx
        y += dy
        pyLogo = Image.open(path)
        fig.add_layout_image(dict(source=pyLogo,
                                  x=x, y=y,
                                  sizex=.2, sizey=.2,
                                  xref="x", yref="y",
                                  xanchor="center" , yanchor ="bottom"
                                  ))
        
    return fig

def _hoover_agent(net, fig):
    fig.update_traces(hovertemplate='TEST', selector={'name':'agent'})
    return fig

def _hoover_LMP(net, fig):
    c1 = net.bus.name
    c2 = net.res_bus.vm_pu
    fig.update_traces(customdata=np.dstack((c1,c2)),
                      hovertemplate='%{customdata[0]} %{x}<br />V_m = :%{customdata[1]} pu<br />V_m = 20.0 kV<br />V_a = 0.0 deg',
                      selector={'name':'buses'})
    return fig

def _hoover_power_flow(net, fig):
    c1 = net.line.name
    c2 = net.line.max_loading_percent
    fig.update_traces(customdata=np.dstack((c1,c2)), hovertemplate='%{customdata[1]}', selector={'name':'lines'})
    return fig

def plot_improved_pf(net, title=None, path_folder=None):
    """Plot the strucure of the network.
    Shows agent placement."""
    fig = pf_res_plotly(net, auto_open=False) # Plot the basic think using the built in function
    # fig = _display_load(net, fig)
    # fig = _display_gen(net,fig)
    fig = _hoover_agent(net, fig)
    fig = _hoover_LMP(net, fig)
    fig = _hoover_power_flow(net, fig)
    
    if title != None :
        fig.write_html(join(path_folder,str(title)+'.html'), auto_open=False)
    else :
        fig.show(auto_open=False)
    return fig

def plot_test_case(net, title=None, path_folder=None):
    """Plot the strucure of the network.
    Shows agent placement."""
    fig = simple_plotly(net, auto_open=False) # Plot the basic think using the built in function
    fig = _display_load(net, fig)
    fig = _display_gen(net,fig)
        
    if title != None :
        fig.write_html(join(path_folder,str(title)+'.html'), auto_open=False)
    else :
        fig.show(auto_open=False)
    return fig

def plot_pf(net, title, path_folder):
    """Compute and display 3 different power flows :
        - Power flow with only load
        - Power flow whith only gen
        - Power flow with both"""
    # Both gen et load at max
    pp.runpp(net, enforce_q_lims=True)
    # fig = plot_improved_pf(net, title=str(title)+'_pf_load_gen', path_folder=path_folder)
    fig = pf_res_plotly(net, auto_open=False)
    fig.write_html(join(path_folder,str(title)+'_pf_load_gen.html'), auto_open=False)
    # No load, maximum gen
    net = modif_net.set_max_gen(net)
    pp.runpp(net, enforce_q_lims=True)
    fig = pf_res_plotly(net, auto_open=False)
    fig.write_html(join(path_folder,str(title)+'_pf_gen.html'), auto_open=False)
    # No gen, maximum load
    net = modif_net.set_max_load(net)
    pp.runpp(net, enforce_q_lims=True)
    fig = pf_res_plotly(net, auto_open=False)
    fig.write_html(join(path_folder,str(title)+'_pf_load.html'), auto_open=False)

####################################################################################
####################################### MAIN #######################################
####################################################################################

def main(folder, path_origin, title):
    """Give a bus number to each agent of the data set and save is into the network.
    Also give a flexibility to each agent."""
    path_folder = join(path_origin, "processed/use/{0}".format(folder))
    net = pp.from_pickle(join(path_folder,"net.p"))
    fig = plot_test_case(net, title=title, path_folder=path_folder)
    try :
        plot_pf(net, title, path_folder)
    except :
        print("Plot impossible, PandaPower could not compute pf. Usally PowerModel can.")
    return fig
    
if __name__ == "__main__":
    """The part of the code will be executed only if THIS file is executed (not if it is call by another file).
    This serves for testing purposes only."""
    
    path_origin , folder = imp.path_import("PC_Gueno_win",0)
    
    fig = main(folder=folder, path_origin = path_origin, title="Network")