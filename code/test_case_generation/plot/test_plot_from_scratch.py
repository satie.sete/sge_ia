# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 19:24:59 2022

@author: Guenole
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandapower as pp
import pandas as pd
import numpy as np
from os.path import join
import os
import matplotlib.image as image
from matplotlib.offsetbox import (OffsetImage, AnnotationBbox)
# Custom import
import code_these.src._import as imp


def _plot_bus(ax, net, bus_color=None, label=None, cbar_label=None, vmin=0.9, vmax=1.1, display_cbar=True):
    """Plot buses with a customisable color map"""
    df = pd.merge(net.bus, net.bus_geodata, left_index=True, right_index=True, suffixes=('', '_bis'))
    df = pd.merge(df, net.res_bus, left_index=True, right_index=True, suffixes=('', '_bis'))
    if label != None :
        df = pd.merge(df, bus_color, left_index=True, right_index=True, suffixes=('', '_bis'))
        try : c = df[label+"_bis"].values
        except : c = df[label].values
        im = ax.scatter(df.x, df.y, edgecolors='none', cmap="viridis", s=20,c=c, vmin=vmin, vmax=vmax)
        minx = min(df.x)
        miny = min(df.y)
        maxx = max(df.x)
        maxy = max(df.y)
        dx = maxx - minx
        dy = maxy - miny
        ax.set(xlim=(minx-dx/40, maxx+dx/40), ylim=(miny-dy/40, maxy+dy/10))
        if display_cbar :
            cbar = plt.colorbar(im)
            cbar.set_label(cbar_label)
    else :
        im = ax.scatter(df.x, df.y, edgecolors='none', cmap="jet", s=10,c="grey", vmin=vmin, vmax=vmax)
    return ax, im

def _plot_line(ax, net, line_color=None, label=None, cbar_label=None, vmin=0, vmax=105):
    """Plot lines with a customisable color map"""
    df_coord = _get_coord_lines(net, net.line["to_bus"], net.line["from_bus"])
    df = pd.merge(net.line.loc[:,["name", "from_bus", "to_bus"]],
                  df_coord,
                  left_index=True, right_index=True, suffixes=('', '_bis'))
    if label != None :
        df = pd.merge(df, line_color, left_index=True, right_index=True, suffixes=('', '_bis'))
        # colormap
        cmap = plt.get_cmap('viridis', vmax)
        for id_line in df.index:
            ax.plot([df_coord.loc[id_line, "x_from"], df_coord.loc[id_line, "x_to"]],
                    [df_coord.loc[id_line, "y_from"], df_coord.loc[id_line, "y_to"]], 
                    zorder=0, c=cmap(int(df.loc[id_line, label])), linewidth=2.5)
        # Normalizer
        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
        # creating ScalarMappable
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])
        cbar = plt.colorbar(sm, ticks=np.linspace(vmin, vmax, 5))
        cbar.set_label(cbar_label)
    else :
        for id_line in df.index:
            ax.plot([df_coord.loc[id_line, "x_from"], df_coord.loc[id_line, "x_to"]],
                    [df_coord.loc[id_line, "y_from"], df_coord.loc[id_line, "y_to"]], zorder=0, c="grey")
    return ax

def _plot_agent(ax, net, size, typ, y_translate=10):
    """Add one type of agent to the plot"""
    def _trace():
        for bus_index in bus :
            x, y = net.bus_geodata.loc[bus_index, ["x", "y"]]
            ab = AnnotationBbox(imagebox, (x+x_translate, y+y_translate), frameon = False)
            ax.add_artist(ab)
            
    x_translate = {"apt":0, "ev": 0, "pv":0, "PCC":0} # {"apt":-3, "ev": 0, "pv":3, "PCC":0}
    x_translate= size*x_translate[typ]
    scale = {"apt":0.002, "ev": 0.016, "pv":0.035, "PCC":0.02}
    path = join(os.path.dirname(__file__),"../../","figures/plotly/"+typ+".png")
    img = image.imread(path)
    imagebox = OffsetImage(img, zoom = size*scale[typ])

    if typ == "PCC": bus = net.ext_grid["bus"]
    else : bus = net.agent_table.loc[net.agent_table["type"] == typ, "bus"]
    _trace()
    return ax

def _plot_all_agents(ax, net, size, translate=10):
    """Add all agents to the plot"""
    typ = ["apt", "ev", "pv", "PCC"]
    y_translate = {"apt":translate, "ev": translate, "pv":translate, "PCC":translate+size*25}
    for agent in typ :
        _plot_agent(ax, net, size, agent, y_translate[agent])
    return ax

def _get_coord_lines(net, to_bus, from_bus):
    ind = net.line.index
    x_to = net.bus_geodata.loc[to_bus, "x"].values
    y_to = net.bus_geodata.loc[to_bus, "y"].values
    x_from = net.bus_geodata.loc[from_bus, "x"].values
    y_from = net.bus_geodata.loc[from_bus, "y"].values
    data = {"x_from":x_from, "y_from":y_from,"x_to":x_to,"y_to":y_to}
    df_coord = pd.DataFrame(data, index=ind)
    return df_coord

def plot_constraints(net, size= 0.4, translate=0, plot_bus=True, plot_line=True, plot_agent=True, save_plot=False):
    fig, ax = plt.subplots(figsize=(8,3), dpi=300)
    if plot_bus : _plot_bus(ax, net, net.res_bus, "vm_pu", "Voltage magnitude (pu)")
    if plot_line : _plot_line(ax, net, net.res_line, "loading_percent", "Loading of lines (in %)")
    if plot_agent : _plot_all_agents(ax, net, size, translate)
    # GENERAL
    fig.patch.set_visible(True) # False for transparent back ground
    ax.axis('off')
    # ax.set_title("Network constrains")
    if save_plot : plt.savefig(join(os.path.dirname(__file__), "../../influence_impedence_on_OPF/fig", "fig_network.pdf"))
    plt.show()

def main():
    plot_constraints(net, 0.8)
    
if __name__ == "__main__":
    """The part of the code will be executed only if THIS file is executed (not if it is call by another file).
    This serves for testing purposes only."""
    path_origin , folder = imp.path_import("PC_Roman",0)

    path_folder = join(path_origin, "processed/use/{0}".format(folder))
    net = pp.from_pickle(join(path_folder,"net.p"))
    pp.runpp(net, enforce_q_lims=True)

    # loading = 100*(net.res_line.loading_percent/net.line.max_loading_percent).values
    
    main()



