# -*- coding: utf-8 -*-
"""
Created on Fri Sep  9 14:06:27 2022

@author: Guenole

Process Data from the EPEX SPOT data set.
Data is 1h time step upsample to 1min to be coherent whith the rest of the data set.
"""

# Strandard libraries
import pandas as pd
import numpy as np
from os.path import join
import os
# Custom import
import code_these.src._import as imp

#######################################################################################
################################### Import PV Data ####################################
#######################################################################################

def process_data(data_dir_path, save_path):
    """Import PV data.
    Time step : 1h (should be upsample)
    Missing data between 9pm and 5am (night so no power produced)"""
    dat = []
    file_path = imp.get_files_path(data_dir_path, '.csv')
    for file in file_path:
        file_save_path = join(save_path, file[:-4])
        data = pd.read_csv(join(data_dir_path, file))
        data = _upsample(data)
        _save_data(data, file_save_path)
        dat.append(data)
    return dat

def _save_data(data, save_path):
    os.makedirs(save_path, exist_ok=True)
    for k in range(365):
        begin = k*24*60
        end = (k+1)*24*60
        np.savetxt(join(save_path, str(k)+".csv"),
                   data.iloc[begin:end,0].values,
                   delimiter=",")

def _upsample(data):
    """Fill the rest of the day with 0 values."""
    data = data.set_index(pd.DatetimeIndex(data.loc[:,"date"]+" "+data.loc[:,"start_hour"]))
    data = data.loc[:,"price_euros_mwh"]
    data.sort_index(inplace=True)
    y = int(data.index.strftime('%Y')[0])
    idx = pd.date_range(str(y)+"-01-01", str(y+1)+"-01-01", freq="min")
    ts = pd.DataFrame(index=idx)
    data = ts.join(data).fillna(method="ffill")
    data = ts.join(data).fillna(0)
    return data

def _get_folders(path_origin):
    data_dir_path = join(path_origin,"raw/Spot_price/data")
    save_dir_path_apt = join(path_origin,"processed/data/EPEX")
    return data_dir_path, save_dir_path_apt

#######################################################################################
######################################## MAIN #########################################
#######################################################################################
    
def main(path_origin):
    """Import and process the UMASS data set.
    Saves the result in a file.
    
    """
    # Import paths
    data_dir_path, save_dir_path_apt = _get_folders(path_origin)
    # Process data
    data = process_data(data_dir_path, save_dir_path_apt)
    return data

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC",None)
    
    data = main(path_origin = path_origin)
    plot = True
    if plot :
        import matplotlib.pyplot as plt
        plt.plot(pd.concat(data)[:1700000])
        plt.title("Electricity market spot price")
        plt.xlabel("Date")
        plt.ylabel("Price (€/MkW)")
        plt.show()
        
    