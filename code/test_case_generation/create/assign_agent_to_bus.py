# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 17:30:34 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Give a bus number to each agent of the data set and save it into the network.
"""
import pandas as pd
from os.path import join
import pandapower as pp
from pandapower import topology
import numpy as np
# Custom import
import code_these.src._import as imp
import code_these.src._modify_net as modif_net
import code_these.src._initialize as initialize
# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence

def assign_agent_to_bus(rs, folder, path_origin, n_agents):
    """Assign a bus to each agent of the network.
    Each leaf bus should host at least one agent.
    
    Parameter
    ---------
    - rs : random sequence generator
    - folder : int
        Id of the folder where testcase is saved.
    - path_origin : str
        Path where data is stored (both times series and vreated test case). By default in the NASS.
    """
    def _get_leaf(net):
        G = topology.create_nxgraph(net) # G is the networkX vision of the net (transfo and line are considered the same way)
        bus_leaf = np.asarray([x for x in G.nodes() if len(list(G.neighbors(x)))==1], dtype=int) # Get all leaf busses (They should all be connected to at least 1 agent)
        bus_leaf = np.delete(bus_leaf, np.where(bus_leaf==0)) # Slack bus is not considered to be a leaf
        return bus_leaf
    
    path_folder = join(path_origin, "processed/use/{0}".format(folder))
    # Import agents
    agent_table = pd.read_csv(join(path_folder, r"agent_table.csv"), delimiter=',')
    # Import net
    net = pp.from_pickle(join(path_folder,"net.p"))
    
    #### Assign agent to bus ####
    # Get the leaf buses
    bus_leaf = _get_leaf(net)
    agent_table, id_network = _create_agent_table(agent_table, n_agents, bus_leaf, rs, net.name)
    # Create agent_prices
    agent_prices = _create_prices(id_network, rs)
    net.agent_table = agent_table
    return net, agent_table, agent_prices

def _create_agent_table(agent_table, n_agents, bus_leaf, rs, net_name=None):
    def _sample_custom(rs, array, n_sample):
        """Every item in array is sampled. The rest of the sample are drawn uniformly."""
        n_array = len(array)
        if n_sample > n_array : 
            out1 = rs.choice(array, n_array, replace=False)
            out2 = rs.choice(array, n_sample-n_array, replace=True)
            return np.concatenate((out1, out2))
        else : return rs.choice(array, n_sample, replace=False)
        
    def _split_buses(rs, bus_leaf, n_agent):
        n_tot = np.sum(n_agent)
        cumsum = np.cumsum(n_agent)
        rs.shuffle(bus_leaf)
        n_leaf = len(bus_leaf)
        bus = []
        bus.append(bus_leaf[:int(n_leaf*cumsum[0]/n_tot)])
        bus.append(bus_leaf[int(n_leaf*cumsum[0]/n_tot): int(n_leaf*cumsum[1]/n_tot)])
        bus.append(bus_leaf[int(n_leaf*cumsum[1]/n_tot): int(n_leaf*cumsum[2]/n_tot)])
        return bus
    
    proportion_agent = np.asarray([n_agents["n_apt"],n_agents["n_ev"],n_agents["n_pv"]])
    number_agent_per_type = [len(agent_table[agent_table["type"]==typ]) for typ in ["apt","ev","pv"]]
    if net_name == "SGE_RL":
        bus_splited = [[1], [1], [1]]
    else :
        bus_splited = _split_buses(rs, bus_leaf, proportion_agent)
    bus_sampled = [_sample_custom(rs, bus_splited[k], number_agent_per_type[k]) for k in range(len(proportion_agent))]
    bus = np.concatenate(bus_sampled)
    agent_table["bus"] = bus
    
    agent_table["id_network"] = None
    id_network = 3*[None]
    n0 = 2
    for k, typ in enumerate(["apt", "ev", "pv"]) :
        n = 0
        for n, bus in enumerate(agent_table.loc[agent_table["type"]==typ, "bus"].unique()):
            agent_table.loc[agent_table["bus"]==bus, "id_network"] = n + n0
        id_network[k] = np.arange(n0, n0+n+1, dtype=int)
        n0 += n+1
    return agent_table, id_network

def _convert_type(typ):
    if typ == "pv":
        return "gen"
    else :
        return "load"

def create_prices(net, rs):
    id_load = np.sort(net.agent_table.loc[(net.agent_table["et"]=="load"), "id_network"].unique()).astype(int)
    id_gen = np.sort(net.agent_table.loc[(net.agent_table["et"]=="gen"), "id_network"].unique()).astype(int)
    df_load = pd.DataFrame()
    df_load["id_network"] = id_load
    df_load[["cp1","cp2"]] = -150*np.ones((len(id_load),2)) - 50*rs.random(size=(len(id_load),2))
    
    df_gen = pd.DataFrame()
    df_gen["id_network"] = id_gen
    df_gen[["cp1","cp2"]] = 6*np.ones((len(id_gen),2)) + 2*rs.random(size=(len(id_gen),2))
    
    df = pd.concat([df_load, df_gen])
    df = df.set_index("id_network")
    return df

def custom_LMP(folder, path_origin, net=None):
    """Only meant to create agent on the custom LMP network.
    Should not be used for other purposes."""
    # Create agent_table
    dic = {"type" : ["apt","apt","apt","apt","pv","pv","pv","pv"],
           "id_data" : [70, 107,6,108,6,24,5,10],
           "bus" : [11,12,10,16,9,13,14,15],
           "min_p_kw" : [0,0,0,0,0,0,0,0],
           "max_p_kw" : [12.5,11.9,9.47,12,20.1,19.6,15.7,15.2],
           "mean_p_kw" : [1,1,1,1,2.9,2.3,1.17,2],
           "id_network" : [2,3,4,5,6,7,8,9],
           "et" : ["load","load","load","load","gen","gen","gen","gen"]}
    agent_table = pd.DataFrame(dic)
    # Create agent_prices
    dic = {"id_network" : [2,3,4,5,6,7,8,9],
           "cp1" : [-80,-99.5,-110,-120,60,60.1,70,79],
           "cp2" : 50*np.asarray([-80,-99.5,-110,-120,60,60.1,70,79])}
    agent_prices = pd.DataFrame(dic)
    if net == None :
        # get network
        path_folder = join(path_origin, "processed/use/{0}".format(folder))
        net = pp.from_pickle(join(path_folder,"net.p"))
    net.agent_table = agent_table
    # Change line max power in order to make congestion appear
    net.line.loc[:,"max_i_ka"] = 0.010 # 0.05 works for power flow
    return net, agent_table, agent_prices

def custom_simplest_network(rs, folder, path_origin, net=None):
    """Only meant to create agent on the simplest network.
    Should not be used for other purposes."""
    # Create agent_table
    dic = {"type" : ["apt", "pv", "pv"],
           "id_data" : [70, 1, 1],
           "bus" : [1, 2, 2],
           "min_p_kw" : [0, 0, 0],
           "max_p_kw" : [1, 1, 1],
           "mean_p_kw" : [1, 1, 1],
           "id_network" : [2, 3, 4],
           "et" : ["load", "gen", "gen"]}
    agent_table = pd.DataFrame(dic)
    # Create agent_prices
    cp1 = rs.normal(-150, 50, 1)[0]
    dic = {"id_network" : [2, 3, 4],
           "cp1" : [cp1, 60, 20],
           "cp2" : [cp1/10, 60, 35]}
    agent_prices = pd.DataFrame(dic)
    if net == None :
        # get network
        path_folder = join(path_origin, "processed/use/{0}".format(folder))
        net = pp.from_pickle(join(path_folder,"net.p"))
    net.agent_table = agent_table
    return net, agent_table, agent_prices

def _create_prices(id_network, rs):
    cp1 = np.concatenate((rs.normal(-150, 50, int(len(id_network[0])+len(id_network[1]))), np.abs(rs.normal(20, 10, len(id_network[2])))))
    dic = {"id_network" : np.concatenate(id_network),
           "cp1" : cp1,
           "cp2" : cp1/10}
    agent_prices = pd.DataFrame(dic)
    return agent_prices

def custom_Eu_network(rs, folder, path_origin, net=None):
    """Only meant to create agent on Eu network (PowerTech2022).
    Should not be used for other purposes."""
    path_folder = join(path_origin, "processed/use/{0}".format(folder))
    if net == None :
        # get network
        net = pp.from_pickle(join(path_folder,"net.p"))
    # Create agent_table
    n_agents = {"n_apt":40, "n_ev":30, "n_pv":30}
    # bus_leaf = [169, 70, 34, 47,83, 103, 74, 178, 208, 264, 225, 249, 234,
    #             219, 289, 176, 314, 320, 327, 290, 201,
    #             387, 342, 388, 349, 337, 406, 629, 563, 502, 611, 562, 682, 676, 619, 639, 618,
    #             882, 899, 886, 906, 898, 835, 780, 813, 755, 896, 900, 702, 701, 778, 830, 861, 
    #             860, 817, 688, 614, 785, 522, 566, 539, 556, 458, 507]
    bus_leaf = [16, 12, 5, 8, 13, 14, 11, 25, 30, 39, 32, 35, 34, 44, 29, 49, 47, 46, 40, 43, 23,
                57, 53, 58, 54, 52, 61, 89, 79, 68, 85, 78, 95, 93, 88, 90,
                70, 63, 76, 74, 72, 111, 86, 97, 115, 120, 121, 109, 100, 101, 130, 127, 116, 107,
                114, 110, 117, 128, 131, 125, 129, 123]
    # Import agents
    agent_table = pd.read_csv(join(path_folder, r"agent_table.csv"), delimiter=',')
    agent_table, id_network = _create_agent_table(agent_table, n_agents, bus_leaf, rs)
    # Create agent_prices
    agent_prices = _create_prices(id_network, rs)
    net.agent_table = agent_table
    return net, agent_table, agent_prices

def custom_Cigre_mv(rs, folder, path_origin, net=None):
    """Only meant to create agent on Cigre_mv (PowerTech2022).
    Should not be used for other purposes."""
    # Create agent_table
    dic = {"type" : ["apt","apt","apt","apt","apt","apt","apt",
                     "ev", "ev",
                     "pv","pv","pv","pv"],
           "id_data" : [70, 107, 6, 108, 12, 34, 63,
                        0, 1,
                        6, 24, 5, 10],
           "bus" : [2, 3, 4, 6, 7, 9, 13,
                    14, 11,
                    14, 8, 10, 11],
           "min_p_kw" : 13*[0],
           "max_p_kw" : 13*[100],
           "mean_p_kw" : 13*[1],
           "id_network" : [2,3,4,5,6,7,8,9,10,11,12,13,14],
           "et" : ["load","load","load","load","load","load","load",
                   "load","load",
                   "gen","gen","gen","gen"]}
    agent_table = pd.DataFrame(dic)
    # Create agent_prices
    cp1 = np.concatenate((rs.normal(-150, 50, 9), np.abs(rs.normal(6, 2, 5))))
    dic = {"id_network" : [2,3,4,5,6,7,8,9,10,11,12,13,14,15],
           "cp1" : cp1,
           "cp2" : cp1/10}
    agent_prices = pd.DataFrame(dic)
    if net == None :
        # get network
        path_folder = join(path_origin, "processed/use/{0}".format(folder))
        net = pp.from_pickle(join(path_folder,"net.p"))
    net.agent_table = agent_table
    # Change line max power in order to make congestion appear
    net.line.loc[:,"max_i_ka"] *= 0.5 # BUG TODO
    return net, agent_table, agent_prices
    
def save_results(net, agent_price, path_origin, folder=0):
    """Save results in the network."""
    save_dir_path = join(path_origin, "processed/use/{0}".format(folder))
    net.dir_path = save_dir_path # Save the path where the network is saved
    pd.to_pickle(net, join(save_dir_path,"net.p"))
    agent_price.to_csv(join(save_dir_path,"agent_prices.csv"), index=True)
    net.agent_table.to_csv(join(save_dir_path,"agent_table.csv"), index=False)

def main(dic, path_origin, folder, plot=False):
    """Give a bus number to each agent of the data set and save is into the network.
    Also give a flexibility to each agent.
    Parameter
    ---------
    - rs : random sequence generator
    - folder : int
        Id of the folder where testcase is saved.
    - path_origin : str
        Path where data is stored (both times series and vreated test case). By default in the NASS.
    - LMP : bool
        Only used to create agent on the LMP network. (default is False)
    """
    network = dic["network"]
    n_agents = dic["n_agents"]
    rs = dic["rs"]
    if network == "LMP" :
        net, agent_table, agent_price = custom_LMP(folder, path_origin)
    elif network == "Eu" :
        net, agent_table, agent_price = custom_Eu_network(rs, folder, path_origin)
        # net, agent_table, agent_price = assign_agent_to_bus(rs, folder=folder, path_origin = path_origin, n_agents=n_agents)
        # net, n_agent = agregate_agent(net, agent_table)
        # agent_price = create_prices(net, rs)
    elif network == "Cigre_mv":
        net, agent_table, agent_price = custom_Cigre_mv(rs, folder, path_origin)
    elif network == "Simplest":
        net, agent_table, agent_price = custom_simplest_network(rs, folder, path_origin)
    else :
        net, agent_table, agent_price = assign_agent_to_bus(rs, folder=folder, path_origin = path_origin, n_agents=n_agents)
    net = modif_net._init_agents(net)
    save_results(net, agent_price, path_origin, folder)
    return net, agent_table
    
    
if __name__ == "__main__":
    """The part of the code will be executed only if THIS file is executed (not if it is call by another file).
    This serves for testing purposes only."""
    
    path_origin , folder = imp.path_import("PC_Gueno_win",2)
    dic = initialize._init_dic()
    
    rs = RandomState(MT19937(SeedSequence(1)))
    net, flex_data = main(dic, path_origin = path_origin, folder=folder, plot=True)
    