# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 17:30:34 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Create agents by agregating house, pv and ev data.
"""
import numpy as np
import pandas as pd
# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
import os
# Custom import
import code_these.src._import as imp
import code_these.src._initialize as initialize

def get_n(data, power, rs, op="mean"):
    """Shuffle the list and select n agent so that the power consumed by these agents is egal to power.
    
    Parameter 
    ---------
    - data : DataFrame
        Contains info on a type of agent (loaded using import_tables)
    - power : float
        Power of the sum of agent selected in table
    - rs : random sequence generator
        Shoud be specify to randomly select agents.
    - op : str ["max", "mean"]
        Select agent according to there "max" or "mean" power.
    Return 
    ---------
    - data_shuffled : DataFrame
        table suffled
    - n : int
        Number of agent to sample to get the requested power
    """
    data_shuffled = data.sample(frac=1, random_state=rs).reset_index(drop=True)
    if op=="max":
        cumsum = data_shuffled["max_p_kw"].cumsum()
    elif op=="mean" :
        cumsum = data_shuffled["mean_p_kw"].cumsum()
    cumsum = cumsum.abs()
    power_max = max(cumsum)
    n_loop = power//power_max
    power = power % power_max
    n = cumsum[cumsum >= power].index.min() + n_loop * len(cumsum)
    if n_loop != 0 :
        print("Each agent appears more then once")
        data_shuffled = pd.concat(int(n_loop+1)*[data_shuffled], ignore_index=True)
    return data_shuffled, int(n)

def create_agents(apt_info, pv_info, ev_info, rs, apt_p_kw=100, ev_percent=0.15, pv_percent=0.3):
    """Create agents based on apt, PV and EV. The table is composed of the folling info :
        - dataid : ID of the created agent
        - id_pv : ID of the PV data
        - id_apt : ID of the apt
        - id_ev : ID of the EV charging station
        - min_p_kw : minimum power of the agent during the simulation
        - max_p_kw : maximum power of the agent during the simulation
        - mean_p_kw : mean power of the agent during the simulation
    
    Parameter
    ---------
    - apt_info : Pandas dataframe
        DataFrame containing loads info.
    - pv_info : Pandas dataframe
        DataFrame containing PV panels info.
    - ev_info : Pandas dataframe
        DataFrame containing EV charging stations info.
    - rs : random sequence generator
        Shoud be specify to randomly create agents.
    - apt_p_kw : float
        Total peak power of all flats (defaul is 100 kW)
    - ev_percent : float
        Ratio of the peak power of ev station and the peak power consumed by load (default is 0.15)
    - pv_percent : float
        Ratio of the peak power of PV panels and the peak power consumed by load (default is 0.3) 
    
    Return
    ---------
    - flex_data : (Panda DataFrame)
        Table containing the info described above.
    """
    # Select which load, EV and PV will will use
    apt_info, n_apt = get_n(apt_info, apt_p_kw, rs, "max") # Select n_load so that the sum of the power consumed by loads is egal to power_kw
    ev_info, n_ev = get_n(ev_info, ev_percent*apt_p_kw/100, rs, "max") # Sample ev charging station
    pv_info, n_pv = get_n(pv_info, pv_percent*apt_p_kw/100, rs, "max") # Sample PV panels
    
    
    # Create a table with the agent going to the network. On agent can be composed of house, PV, and/or charging station.
    agent_table = pd.DataFrame(columns = ["type", "id_data", "bus", "min_p_kw", "max_p_kw", "mean_p_kw"])

    _assign_table(agent_table, apt_info, "apt", n_apt, 0)
    _assign_table(agent_table, ev_info, "ev", n_ev, n_apt)
    _assign_table(agent_table, pv_info, "pv", n_pv, n_apt+n_ev)

    return agent_table

def _assign_table(agent_table, info, typ, n, n_passed):
    for k in range(n):
        agent_table.loc[k+n_passed, ["id_data", "min_p_kw", "max_p_kw", "mean_p_kw"]] = info.loc[k, ["id_data", "min_p_kw", "max_p_kw", "mean_p_kw"]]
        agent_table.loc[k+n_passed, ["type"]] = typ
        if (typ == "apt") or (typ == "ev") : et = "load"
        else : et = "gen"
        agent_table.loc[k+n_passed, ["et"]] = et

def save_to_csv(path_origin, folder, agent_table):
    """Save results in csv files."""
    save_dir_path = os.path.join(path_origin, "processed/use", str(folder))
    os.makedirs(save_dir_path, exist_ok=True)
    agent_table.to_csv(os.path.join(save_dir_path,"agent_table.csv"), index=False)

def main(dic, path_origin, folder):
    """
    Create agents by agregating house, pv and ev data.
    Agents are then saved.
    
    Parameter
    ---------
    - rs : random sequence generator
        Shoud be specify to create agents.
    - apt_p_kw : float
        Total peak power of all flats (default is 100 kW)
    - ev_percent : float
        Ratio of the peak power of ev station and the peak power consumed by load (default is 0.15)
    - pv_percent : float
        Ratio of the peak power of PV panels and the peak power consumed by load (default is 0.3) 
    
    Return
    -------
    agent_table : Panda Dataframe
        Statistic on agent power exchanged
    id_apt, id_pv, id_ev : Panda Dataframe
        Which house, pv, ev is connected to the agent
    """
    apt_info, pv_info, ev_info = imp.import_tables(path_origin)
    agent_table = create_agents(apt_info, pv_info, ev_info, dic["rs"], 
                                apt_p_kw=dic["apt_p_kw"], 
                                ev_percent=dic["ev_percent"], pv_percent=dic["pv_percent"])
    save_to_csv(path_origin, folder, agent_table)
    return agent_table
    
if __name__ == "__main__":
    """The part of the code will be executed only if THIS file is executed (not if it is call by another file).
    This serves for testing purposes only."""
    path_origin , folder = imp.path_import("PC_Gueno_win",0)
    dic = initialize._init_dic()
    agent_table = main(dic, path_origin, folder)