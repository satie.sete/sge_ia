#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 17:34:06 2023

@author: gcherot
"""

# Standard library
from stable_baselines3.common.env_checker import check_env
import numpy as np
import matplotlib.pyplot as plt
# Custom import
from code_these.RL_prix_nodaux.agents import Agent_random, Agent_random_deterministic, Agent_no_action, Agent_opti
import code_these.src._import as imp
import code_these.RL_prix_nodaux.compute as compute
import code_these.RL_prix_nodaux.use as use

def test_opti_agent(path_origin, folder):
    def _plot(env, obj):
        X=np.zeros(100)
        action = np.linspace(-1,1,100)
        for k, a in enumerate(action):
            env.step([[a]])
            env_to_modif = env.get_attr("env")[0]
            env_to_modif.t -= 1
            X[k] = np.abs(np.abs(env_to_modif.power_exchanged) - env_to_modif.dic["line_capacity"])
        plt.plot(action, X, label="function")
        plt.xlabel("action")
        plt.ylabel("||Power exchanged| - line capa|")
        plt.plot(action, obj*np.ones(100), label="opti")
        plt.legend()
        plt.show()
        
    env = use._import_env(path_origin, folder)
    agent = Agent_opti(env)
    env_to_modif = env.get_attr("env")[0]
    for day in range(10):
        env_to_modif._set_day_test(day)
        obs = env.reset()
        for t in [0,5*60,10*60,15*60]:
            env_to_modif.t = t
            action, info = agent.predict(obs)
            _plot(env, info[0])
    
def test_agent(path_origin, folder, agent_name):
    env = use._import_env(path_origin, folder)
    agent = agent_name(env)
    env.reset()
    env.t = 12*60
    obs, _, _, _ = env.step([[-1]])
    action, _ = agent.predict(obs)
    env.step(action)
    env.get_attr("plot")[0]()

def test_plot_info(path_origin, folder):
    env = use._import_env(path_origin, folder)
    agent = Agent_random(env)
    ep = 1
    compute.plot_infos(agent, n_episode=ep, n_jobs=ep)
    
def test_all_agents(path_origin, folder):
    agent_name = [Agent_random, Agent_random_deterministic, Agent_no_action, Agent_opti]
    for ag in agent_name :
        test_agent(path_origin, folder, ag)
        
def test_merit_order_plot(path_origin, folder):
    env = use._import_env(path_origin, folder, vec=False)
    env.set_seed_day(50)
    env.price_DSO = 0.12
    for t in [0,5*60,11.2*60,16*60]:
        env.t = int(t)
        # env.plot()
        env._plot_merit_order(gimp=True)
            
def test_check_env(path_origin, folder):
    env = use._import_env(path_origin, folder, vec=False)
    check_env(env, warn=True)

def main(path_origin, folder):
    # test_opti_agent(path_origin, folder)
    # test_all_agents(path_origin, folder)
    test_plot_info(path_origin, folder)
    test_merit_order_plot(path_origin, folder)
    test_check_env(path_origin, folder)
    pass
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Gueno_win", 5)
    main(path_origin, folder)
    dic = imp.load_dict(folder, path_origin, "../dict_parameters")