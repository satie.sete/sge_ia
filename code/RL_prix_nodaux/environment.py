# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 11:10:02 2021

@author: Guenole CHEROT
"""
# Standard libraries
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import os
from os.path import join
import pickle
import gym
from gym import spaces
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.gridspec as grd
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
# Custom import
import code_these.src._import as imp
import code_these.RL_prix_nodaux.compute as compute


class Env(gym.Env):
    """
    Custom Environment that follows gym interface"""
    
    metadata = {'render.modes': ['human']}
    
    ########################################################################################################
    ################################################# INIT #################################################
    ########################################################################################################
    
    def __init__(self, dic,  path_origin, folder):
        """
        Parameters
        ----------
        
        """
        # Init from mother class
        super(Env, self).__init__()
        # Usefull general variables 
        self.save_dir_path = join(path_origin, "processed/use", str(folder))
        self.data_path = join(path_origin, "processed/data")
        self.dic = dic
        # self.dic["number_of_time_step"] = 10 # BUG
        self.agent_table = pd.read_csv(join(self.save_dir_path, 'agent_table.csv'))
        ######################################## INIT AS A GYM ENV ########################################
        # Define the action space
        self.action_space = spaces.Box(low=np.array([-1]), high = np.array([1]))
        # Define the observation space :
            # - time (day is an hidden variable)
            # - power last time step
            # - price broadcasted last time step (last action)
            # - grid price last time step
        self.n_obs = 6
        self.observation_space = spaces.Box(low=-np.ones(self.n_obs), high=np.ones(self.n_obs))
        self.obs_name = ["Temps","Puissance réseau","Puissance communauté","Prix GRD","Prix réseau","Prix final"]
        
        #################################### Init the other parameters #################################### 
        self.init_env()
    
    def _sample_day(self):
        if self.dic["mode"] == "train":
            return self.dic["seed_day"].choice(self.dic[self.dic["mode"]+"_days"])
        else :
            try :
                self.day_test = (self.day_test + 1)%len(self.dic[self.dic["mode"]+"_days"])
            except :
                self._set_day_test(0)
            return self.dic[self.dic["mode"]+"_days"][self.day_test]
        
    def init_env(self):
        self.t = self.dic["t_init"]
        self.day = self._sample_day()
        self.power_max_load = self.agent_table.loc[self.agent_table["et"]=="load"].sum()["max_p_kw"]
        self.power_max_gen = self.agent_table.loc[self.agent_table["et"]=="gen"].sum()["max_p_kw"]
        self.power_max = np.maximum(self.power_max_load, self.power_max_gen)
        self.grid_price_max = None
        self.agent_price_max = self.dic["non_flex_price"]*self.agent_table.max()["price"]
        self.data = imp.import_power_as_dataframe(self.agent_table, self.day, self.data_path)
        self.data_grid_price = 1e-3*imp.import_epex(self.day, self.data_path, self.dic["epex_year"])
        
        self.step([0])
        
    ########################################################################################################
    ################################################ SETERS ################################################
    ########################################################################################################
    
    def master_set(self, dic):
        self.dic = dic
        self.init_env()
    
    def set_seed_day(self, seed_state):
        self.dic["rd_state_day"] = seed_state
        self.dic["seed_day"] = RandomState(MT19937(SeedSequence(seed_state)))
        self.init_env()
        
    def _set_day_test(self, day):
        self.day_test = day
        rs = RandomState(MT19937(SeedSequence(0)))
        rs.shuffle(self.dic[self.dic["mode"]+"_days"])
        
    def get_dic(self):
        return self.dic
    
    def get_infos(self):
        """
        Retrun
        ----------
        - Time
        - Power exchanged through line
        - Power exchanged in community
        - Price imposed by DSO
        - Grid Price
        - Final nergy price
        """
        out = np.asarray([self.t,
                          self.power_exchanged,
                          self.power_community,
                          self.price_DSO,
                          self.grid_price,
                          self.energy_price])
        return out
    
    ########################################################################################################
    ############################################ Gym interface #############################################
    ########################################################################################################

    def reset(self):
        """Reset the environement and warm stats it.
        
        Parameters
        ----------
       
        Retrun
        ----------
        obs : array
            Observation of the network state
        """
        self.init_env()
        return self.get_obs()
        
    def step(self, action):
        """Step in the environnement
        
        Parameters
        ----------
        action : 
            
        Retrun
        ----------
        obs : array
            Observation of the network state
        reward : float
            Reward obtained after the step
        done : bool
            True if the simulation as ended
        info : dict
            Information about the result of that step"""
        self.action = action[0]
        # rescale to help SAC
        ac = np.clip(self._inverse_rescale(action[0])*1.1-0.1,0,1)
        self.price_DSO = self.dic["price_DSO_max"]*ac
        self.compute_obs()
        obs = self.get_obs() # Get the network observation
        reward = self.get_reward() # Get the reward (can be changed after in a wrapper)
        done = self.get_done()
        info = self.get_info()
        self.t += 1
        return obs, reward, done, info
    
    def compute_obs(self):
        self.compute_power_from_action()
    
    def compute_merit_order(self, price_DSO, ext_grid=True):
        def _initialize():
            power_t = self.data[str(self.t)].values
            price_t = self.agent_table.loc[:,"price"].values
            grid_price_t = self.data_grid_price[self.t]
            self.grid_price = grid_price_t
            # Little cost to add to be convex
            convex_cost = -np.ones(len(self.agent_table))*self.dic["delta_convex"]
            return power_t, price_t, grid_price_t, convex_cost
        
        def _compute_cost():
            power = np.concatenate((self.dic["non_flex_power"]*power_t,
                                    0*power_t, 
                                    self.dic["flex_power"]*power_t,
                                    0*power_t,
                                    [0, self.power_max, 0, self.power_max]))
            cost = np.concatenate((price_t,
                                   price_t+convex_cost, 
                                   self.dic["non_flex_price"]*price_t,
                                   self.dic["non_flex_price"]*price_t+convex_cost,
                                   [grid_price_t-price_DSO-self.dic["delta_convex"],
                                    grid_price_t-price_DSO,
                                    grid_price_t+price_DSO-self.dic["delta_convex"]+self.dic["delta_sell_buy"],
                                    grid_price_t+price_DSO+self.dic["delta_sell_buy"]]))
            index = np.arange(len(self.agent_table))
            index = np.concatenate((index,index,index,index,[-1, -2, -3, -4]))
            if ext_grid :
                all_price = pd.DataFrame(data={"index":index, "power":power, "cost":cost})
            else :
                all_price = pd.DataFrame(data={"index":index[:-4], "power":power[:-4], "cost":cost[:-4]})
            return all_price
        
        def _merit_order():
            index_agent_ordered = all_price.sort_values("cost").index
            power = all_price["power"].to_numpy(dtype=float)[index_agent_ordered]
            cost = all_price["cost"].to_numpy(dtype=float)[index_agent_ordered]
            idex = all_price["index"].to_numpy(dtype=int)[index_agent_ordered]
            et = self.agent_table["et"].to_numpy(dtype=str)[idex]
            isin_load = np.isin(idex, [-1,-2])
            isin_gen = np.isin(idex, [-3,-4])
            ext = isin_load | isin_gen
            et[isin_load] = "load"
            et[isin_gen] = "gen"
            
            power = {"gen":np.cumsum(power[et=="gen"]), "load":np.cumsum(power[et=="load"])}
            price = {"gen":cost[et=="gen"], "load":cost[et=="load"]}
            ext_grid = {"gen":ext[et=="gen"], "load":ext[et=="load"]}
            return power, price, ext_grid
        
        def _flip():
            power["load"] = np.max(power["load"]) - power["load"]
            power["load"] = power["load"][::-1]
            price["load"] = price["load"][::-1]
            ext_grid["load"] = ext_grid["load"][::-1]
        
        power_t, price_t, grid_price_t, convex_cost = _initialize()
        all_price = _compute_cost()
        power, price, ext_grid = _merit_order()
        _flip()
        return power, price, ext_grid

    def compute_power_from_action(self, ext_grid=True):
        self.merit_order_power_2, self.merit_order_price_2, self.merit_order_ext_2 = self.compute_merit_order(self.price_DSO, ext_grid)
        # compute the power exchanged
        # Where lines cross
        crossing_load = compute.find_curve_crossings(self.merit_order_power_2["load"], self.merit_order_price_2["load"],
                                                     self.merit_order_power_2["gen"], self.merit_order_price_2["gen"])
        crossing_gen = compute.find_curve_crossings(self.merit_order_power_2["gen"], self.merit_order_price_2["gen"],
                                                    self.merit_order_power_2["load"], self.merit_order_price_2["load"])
        # Does ext_grid play a role
        if self.merit_order_ext_2["load"][crossing_load[0]] :
            self.power_community = self.merit_order_power_2["load"][crossing_load[0]]
            power_total = self.merit_order_power_2["gen"][crossing_gen[0]]
            self.energy_price = self.merit_order_price_2["load"][crossing_load[0]]
            sign = -1
        elif self.merit_order_ext_2["gen"][crossing_gen[0]] :
            self.power_community = self.merit_order_power_2["gen"][crossing_gen[0]]
            power_total = self.merit_order_power_2["load"][crossing_load[0]]
            self.energy_price = self.merit_order_price_2["gen"][crossing_gen[0]]
            sign = 1
        else :
            power_total = self.power_community = self.merit_order_power_2["load"][crossing_load[0]]
            self.energy_price = self.merit_order_price_2["load"][crossing_load[0]]
            sign = 0
        # Déduire puissance échanger dans le réseau et en dehors
        self.power_exchanged = sign*(power_total - self.power_community)
    
    def get_obs(self):
        obs = np.zeros(self.n_obs, dtype=np.float32)
        obs[0] = self.t/self.dic["number_of_time_step"]
        # obs[1] = self._inverse_rescale(np.sign(self.power_exchanged)*np.clip(np.abs(self.power_exchanged) - self.dic["line_capacity"], 0, None)/self.power_max)
        obs[1] = self._inverse_rescale(self.power_exchanged/self.power_max)
        obs[2] = self.power_community/self.power_max
        obs[3] = self.price_DSO/self.dic["price_DSO_max"]
        obs[4] = self.grid_price/self.agent_price_max
        obs[5] = self.energy_price/self.agent_price_max
        self.obs = self._rescale(obs)
        return self.obs
    
    def get_reward(self):
        if np.abs(self.power_exchanged) > self.dic["line_capacity"] :
            return -self.dic["alpha_congestion"] * (np.abs(self.power_exchanged) - self.dic["line_capacity"])/self.power_max
        else :
            return self.dic["alpha_exchange"]*np.abs(self.power_exchanged)/self.power_max
    
    def get_done(self):
        if self.t == self.dic["number_of_time_step"] -1:
            return True
        else : return False
    
    def get_info(self):
        return {"info":"None"}
        
    def close(self):
        """Closes the environement.
        TODO"""
        pass
    
    def _rescale(self, data):
        """From [0,1] to [-1, 1]"""
        return (2*data)-1
    
    def _inverse_rescale(self, data):
        """From [-1, 1] to [0,1]"""
        return (data+1)/2
    
    ########################################################################################################
    ################################################ PLOTS #################################################
    ########################################################################################################
    
    def plot(self):
        fig, axs = plt.subplots(2, figsize=(6,5), dpi=300, constrained_layout=True)
        gs = grd.GridSpec(2, 1, height_ratios=[5,2], hspace=0.4)
        ax1 = plt.subplot(gs[0])
        ax2 = plt.subplot(gs[1])
        self._plot_state(ax2)
        self._plot_merit_order(ax1)
        plt.show()
    
    def _plot_state(self, ax):
        ax.bar(np.arange(0,len(self.obs)), self.obs+1, bottom=-1)
        ax.set_xticks(np.arange(0,len(self.obs)), self.obs_name, rotation=30)
        # ax.set_xlabel("Observations")
        ax.set_ylabel("Valeur normé")
        ax.set(ylim=(-1,1))
        ax.grid()
    
    def _plot_merit_order(self, ax=None, folder=None, path_origin=None, gimp=False):
        if ax == None :
            legend = True
            fig, ax = plt.subplots(figsize=(6,3), dpi=300)
        else : legend = False
        def _typ_to_legend(typ):
            if typ=="gen" :
                return "Producteurs"
            else : return "Consommateurs"
            
        def _plot(typ, data_1, data_2, color, style, label=None):
            if label != None : label = _typ_to_legend(typ)
            ax.plot(data_1[typ], data_2[typ], # label=label,
                    color=color, linestyle=style)
        
        merit_order_list = []
        price_DSO = [0,0, self.price_DSO]
        grid = [False, True, True]
        for k in range(len(grid)):
            merit_order_list.append(self.compute_merit_order(price_DSO[k], ext_grid=grid[k]))
        
        
        self.price_DSO = price_DSO[-1]
        self.compute_power_from_action(ext_grid=grid[-1])
        
        style = ["dotted", "dashed", "solid"]
        label = [None, None, None]
        color = plt.rcParams['axes.prop_cycle'].by_key()['color']
        for k, mo in enumerate(merit_order_list):
            _plot("gen", mo[0], mo[1], color[0], style[k], label[k])
            _plot("load", mo[0], mo[1], color[1], style[k], label[k])
        
        if legend:
            h1 = [plt.plot([], color=color[0])[0], plt.plot([], color=color[1])[0]]
            legend1 = plt.legend(h1, ["Générateur", "Consommateur"], loc="upper right", bbox_to_anchor=(.999, 0.99))
            ax.add_artist(legend1)
            
            h2 = [plt.plot([], linestyle= "dotted", color="grey")[0],
                  plt.plot([], linestyle= "dashed", color="grey")[0],
                  plt.plot([], linestyle= "solid", color="grey")[0]]
            legend2 = plt.legend(h2, ["Communauté seule", "Communauté + réseau", "Communauté + réseau + GRD"],
                                  loc="upper right", bbox_to_anchor=(.65, 0.99))
            ax.add_artist(legend2)
            
            if self.power_exchanged != 0 :
                # Line capacity rectangle
                pos_x = self.power_community
                longeur = min(np.abs(self.power_exchanged), self.dic["line_capacity"])
                hauteur = 0.02
                pos_y = self.grid_price + np.sign(self.power_exchanged)*self.price_DSO - hauteur/2
                ax.add_patch(Rectangle((pos_x, pos_y), longeur, hauteur, facecolor=color[2], zorder=-1, alpha=0.4))
                ax.annotate("Capacité ligne", xy=(pos_x+longeur/4,pos_y+0.03),  xytext=(pos_x+longeur/4,pos_y+0.08),
                            arrowprops=dict(color=color[2], lw=1, arrowstyle='-|>'),
                            ha='center', size=8, color=color[2])
                
                # Line exchanged
                longeur2 = np.abs(self.power_exchanged) - longeur
                if longeur2 > 0 :
                    ax.add_patch(Rectangle((pos_x+longeur, pos_y), longeur2, hauteur, facecolor=color[3], zorder=-1, alpha=0.3))
                    ax.annotate("Congestion", xy=(pos_x+longeur+longeur2/2,pos_y+0.03),  xytext=(pos_x+longeur/1.3,pos_y+0.08),
                                arrowprops=dict(color=color[3], lw=1, arrowstyle='-|>',
                                                connectionstyle="angle,angleA=180,angleB=90,rad=5"),
                                ha='center', size=8, color=color[3])
                    
                # to be place in GIMP
                if gimp == True:
                    for k in range(3):
                        ax.plot([10+k*10],[0.1],'.', color=color[k+4])
                    ax.text(10,0.15,r"$+\delta$")
                    ax.text(20,0.15,r"$-\delta$")
            
            
        ax.set_xlabel("Puissance (en kW)")
        ax.set_ylabel("Prix (en €/kW)")
        ax.set_xlim((0,1.1*(np.maximum(merit_order_list[1][0]["gen"][-1], merit_order_list[1][0]["load"][-1])-self.power_max)))
        ax.set_ylim((0,None))
        ax.set_title("Ordre de mérite")
    
        if folder != None :
            imp.save_fig(folder, path_origin, "merit_order")
        else :
            plt.show()
    
    
    
    
    
    
    
    
    
    