#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 17:34:06 2023

@author: gcherot
"""

# Standard library
from stable_baselines3.common.env_checker import check_env
from stable_baselines3 import PPO, SAC
import numpy as np
import matplotlib.pyplot as plt
from os.path import join
# Custom import
from code_these.RL_prix_nodaux.agents import Agent_random, Agent_random_deterministic, Agent_no_action, Agent_opti
import code_these.src._import as imp
import code_these.RL_prix_nodaux.compute as compute
import code_these.RL_prix_nodaux.use as use

def get_time_serie(path_origin, folder):
    def _get(path_origin, folder, agent_name):
        env = use._import_env(path_origin, folder)
        if (agent_name == SAC) or (agent_name == PPO) :
            path = join(path_origin, "processed/use/",
                        "data_fig_1/data/0/data/RL/checkpoint" ,
                        "SAC_2_4999800_steps.zip")
            agent = agent_name.load(path, env)
        else :
            agent = agent_name(env)
        env.reset()
        env_to_modif = env.get_attr("env")[0]
        return compute.evaluate_episode(agent, 0)
    
    agent_names = [Agent_opti, SAC ,Agent_no_action]
    legend_list = ["Optimal", "SAC", "Sans GR"]
    reward = []
    power_ex = []
    price = []
    for agent_name in agent_names :
        _, rew, info = _get(path_origin, folder, agent_name)
        reward.append(rew)
        power_ex.append(info[:,1])
        price.append(info[:,3])
        
    env = use._import_env(path_origin, folder)
    line_capa = env.get_attr("dic")[0]["line_capacity"]
    return power_ex, reward, price, legend_list, line_capa
    
def plot(power_ex, reward, price, legend_list, line_capa):
    color = plt.rcParams['axes.prop_cycle'].by_key()['color']
    green = color[2]
    color = color[0:2]+color[3:]
    def _plot(ax, data, legend, ylabel, zorder):
        for k, data_agent in enumerate(data) :
            ax.plot(data_agent, label=legend[k],color=color[k],zorder=zorder[k])
            ax.set_ylabel(ylabel)
            ax.set_xlim(0, len(data_agent))
            ax.set_xticks(np.arange(0,60*24,3*60), np.arange(0,24,3).astype(str))
            ax.grid()
        if ylabel == "Puissance \n échangée":
            ax.plot([0, len(data_agent)], [line_capa, line_capa], "--", label="Capacité ligne", color=green)
            ax.plot([0, len(data_agent)], [-line_capa, -line_capa], "--", color=green)
    
    fig, axs = plt.subplots(3,figsize=(6, 3*1.5), dpi=300, sharex=True)
    data_list = [power_ex, reward, price]
    ylabel_list = ["Puissance \n échangée", "Récompense", "Coût (€/kW)"]
    zorder=[10,8,9]
    for j, data in enumerate(data_list):
        _plot(axs[j], data, legend_list, ylabel_list[j], zorder)
    axs[0].legend()
    axs[-1].set_xlabel("Temps (en h)")
    plt.tight_layout()
    # plt.show()
    imp.save_fig(folder, path_origin, "series_temporelles")
    
def main(path_origin, folder):
    power_ex, reward, price, legend_list, line_capa = get_time_serie(path_origin, folder)
    plot(power_ex, reward, price, legend_list, line_capa)
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Gueno_win", 5)
    main(path_origin, folder)