#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 17:34:06 2023

@author: gcherot

This code can be run on one are multiple computers.
Data collected should be stored in a folder name : "data_electronique/data/'PC_ID'"
"""

# Standard library
import os
from os.path import join
from stable_baselines3 import PPO, SAC
# Custom import
import code_these.src._import as imp
import code_these.RL_prix_nodaux.use as use
import code_these.RL_prix_nodaux.benchmark as benchmark
import code_these.src._initialize as _initialize

def main(path_origin, folder, n_training_envs, total_timesteps):
    dic = _initialize._init_dic_RL()
    train_env, eval_env = use._create_vectorised_envs(path_origin, folder, dic)
    tensorboard_log_dir = join(path_origin, "processed/use", str(folder), "data/RL/tensorboard/")
    os.makedirs(tensorboard_log_dir, exist_ok=True)
    models = [PPO, SAC]
    models_name = ["PPO", "SAC"]
    for k, m in enumerate(models):
        for j in range(2):
            model = m("MlpPolicy", train_env, verbose=1, tensorboard_log=tensorboard_log_dir)
            callback = use._create_callbacks(path_origin, folder, total_timesteps,
                                                     n_training_envs, eval_env, models_name[k]+"_"+str(j+1))
            model.learn(total_timesteps=total_timesteps, callback=callback)
    benchmark.bench_fig1(path_origin, folder)
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_electronique", "data_train")
    n_training_envs = 12
    total_timesteps = 5_000_000
    fun = lambda x : main(path_origin, folder, n_training_envs, total_timesteps)
    use._try_vec_env(fun, n_try_max=20)