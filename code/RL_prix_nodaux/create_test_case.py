#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: gcherot

Compute OPF on a test case and store dual and primal value.
The simulation is on the hole year.
"""

# Standard library
import os
from os.path import join
import pandas as pd
import pickle
import numpy as np
# Custom import
import code_these.src._import as imp
import code_these.src._initialize as initialize

def typ_to_et(typ):
    if typ == "apt" or typ == "ev":
        return "load"
    else : return "gen"

def sample_agent(dic, path_origin, folder):
    apt_info, pv_info, ev_info = imp.import_tables(path_origin)
    data = {"apt":apt_info, "ev":ev_info, "pv":pv_info}
    agent_table = pd.DataFrame(columns = ["type", "et", "id_data", "bus", "min_p_kw", "max_p_kw", "mean_p_kw"])
    for typ in dic["n_agents"].keys():
        id_data_agents = dic["seed"].choice(len(data[typ]), dic["n_agents"][typ], replace=False)
        for k in id_data_agents :
            dat = data[typ].iloc[k]
            df = pd.DataFrame(data = {"type":typ,
                                      "et":typ_to_et(typ),
                                      "id_data": dat["id_data"],
                                      "bus": 1,
                                      "min_p_kw": dat["min_p_kw"],
                                      "max_p_kw": dat["max_p_kw"],
                                      "mean_p_kw": dat["mean_p_kw"]}, index=[0])
            agent_table = pd.concat([agent_table , df], ignore_index=True)
    agent_table = pd.concat([agent_table , df], ignore_index=True)
    return agent_table

def assign_prices(dic, path_origin, folder, agent_table):
    for k in agent_table.index :
        typ = agent_table.iloc[k].type
        price = np.clip(dic["seed"].normal(dic["mean_price"][typ], dic["std_price"][typ]),0, None)
        agent_table.loc[k, "price"] = price
    return agent_table

def save(dic, path_origin, folder, agent_table):
    save_dir_path = join(path_origin, "processed/use", str(folder))
    os.makedirs(save_dir_path, exist_ok=True)
    agent_table.to_csv(join(save_dir_path,"agent_table.csv"), index=False)
    with open(join(save_dir_path, 'dict_parameters.pkl'), 'wb') as fp:
        pickle.dump(dic, fp)

def main(path_origin, folder, dic):
    agent_table = sample_agent(dic, path_origin, folder)
    agent_table = assign_prices(dic, path_origin, folder, agent_table)
    save(dic, path_origin, folder, agent_table)
    return dic
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman", 5)
    dic = initialize._init_dic_RL()
    main(path_origin, folder, dic)
    