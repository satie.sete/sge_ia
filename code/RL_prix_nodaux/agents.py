#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 11:28:08 2023

@author: gcherot
"""

import numpy as np
from tqdm import tqdm

class Agent_abstract():
    """Agent that takes the role of the system operator of the network.
    He is able to change the price of each transaction in order to change agents consumption and line loading percent. \n
    
    This class is an abstract class."""
    
    ########################################################################################################
    ########################################### INIT and  SETERS ###########################################
    ########################################################################################################
    
    def __init__(self, env):
        self.env = env
        pass
        
    def train(self, env, n_step):
        """Train the agent on a particular env.
        
        Parameter
        ---------
        env :
            Environement on wich action are taken
        n_step : int
            Number of steps used to evaluate the agent performance"""
        raise ValueError("train method not implemented") # Not implemented (astract method, not all agent are able to learn from experience)
    
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        action = self.pred(obs)
        return [action], [None]
        
    def pred(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        raise ValueError("act method not implemented") # (Abstract method)
        
    def test_env(self, env, n_step):
        """Apply the strategy on the env and retrun any error
        
        Parameter
        ---------
        env :
            Environement on wich action are taken
        n_step : int
            Number of steps used to evaluate the agent performance.
            
        Return
        ---------
        info :
            Information on the test"""
            
        obs = env.reset()[0]
        for k in tqdm(range(n_step)):
            action = self.predict(obs)
            obs, reward, done, info = env.step(action)
            if done :
                obs = env.reset()[0]

class Agent_random(Agent_abstract):
    def pred(self, obs, *args, **kwargs):
        return self.env.action_space.sample()
    
class Agent_random_deterministic(Agent_abstract):
    def pred(self, obs, *args, **kwargs):
        dic = self.env.get_attr("dic")[0]
        return dic["seed"].uniform(self.env.action_space.low, self.env.action_space.high)
    
class Agent_no_action(Agent_abstract):
    def pred(self, obs, *args, **kwargs):
        return self.env.action_space.low
        
class Agent_max_action(Agent_abstract):
    def pred(self, obs, *args, **kwargs):
        return self.env.action_space.high
        
class Agent_Smart(Agent_abstract):
    def pred(self, obs, *args, **kwargs):
        power_exchanged = self.env.get_attr("power_exchanged")[0]
        power_max = self.env.get_attr("power_max")[0]
        dic = self.env.get_attr("dic")[0]
        obs = obs[0]
        a = np.sign(power_exchanged)*np.clip(np.abs(power_exchanged) - dic["line_capacity"], 0, None)/power_max
        action = np.clip(obs[3]+0.5*a-0.02, self.env.action_space.low, self.env.action_space.high)
        return action
        
class Agent_opti(Agent_abstract):
    def pred(self, obs, *args, **kwargs):
        def _interogate(action, env):
            env.step(action)
            env.t -= 1
            return np.abs(env.power_exchanged)
        
        def _minimize(fun, x0 = np.copy(self.env.action_space.low), step = (self.env.action_space.high - self.env.action_space.low)/100):
            f = fun(x0)
            while f > env.dic["line_capacity"] :
                x0 += step
                f = fun(x0)
            return x0, f
                
        env = self.env.get_attr("env")[0]
        fun = lambda action : _interogate(action, env)
        x0, best_f = _minimize(fun)
        return x0, best_f
    
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        action, info = self.pred(obs)
        return [action], [info]
        
class Agent_nearly_opti(Agent_opti):
    """Predict the price at the next time step based on the current time step (not omniscient)"""
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        env = self.env.get_attr("env")[0]
        env.t -= 1 # Make the prevision based on the last time step
        action, info = self.pred(obs)
        env.t += 1 # Restore the original time
        return [action], [info]
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        