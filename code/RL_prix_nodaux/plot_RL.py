# -*- coding: utf-8 -*-
"""
Created on Tue May 23 19:09:30 2023

@author: Guenole
"""
# Standard libraries
import os
from os.path import join
os.environ['KMP_DUPLICATE_LIB_OK']='True' # Prevent crash when using plt
import numpy as np
import matplotlib.pyplot as plt
import pickle
# Custom import
import code_these.src._import as imp
import code_these.RL_prix_nodaux.use as use

def _import_data(path_origin, folder, n_PC=6, with_dic=True, transfer = "No"):
    def _import_from_PC(path_origin, folder, PC_ID, data):
        if transfer == "transfer" :
            path = join(path_origin,"processed/use",str(folder), "data",str(PC_ID),"data_transfert/transfert/data/RL/eval/")
        elif transfer == "from_scratch" :
            path = join(path_origin,"processed/use",str(folder), "data",str(PC_ID),"data_transfert/data/RL/eval/")
        else :
            path = join(path_origin,"processed/use",str(folder), "data",str(PC_ID),"data_transfert/data/RL/eval/")
        directories = [name for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))]
        for d in directories :
            file = np.load(join(path, d,"evaluations.npz"))
            data.append({d:{"reward":file["results"], "timesteps":file["timesteps"]}})
        return data

    PC_IDs = np.arange(n_PC)
    data = []
    for PC_ID in PC_IDs :
        data = _import_from_PC(path_origin, folder, PC_ID, data)
    
    if with_dic:
        if transfer == "transfer":
            dir_path = join(path_origin, "processed/use", str(folder), "data/0/data_transfert/data/bench_hist.pkl")
        elif transfer == "from_scratch" :
            dir_path = join(path_origin,"processed/use",str(folder),  "data/0/data_transfert/data/bench_hist.pkl")
        else :
            dir_path = join(path_origin,"processed/use",str(folder),  "data/0/data_transfert/data/bench_hist.pkl")
        with open(dir_path, 'rb') as f:
            dic = pickle.load(f)
    else : dic = None
    return data, dic

def _process_data(data):
    data_processed = {"PPO":[], "SAC":[]}
    time = {"PPO":[], "SAC":[]}
    for d in data :
        key = list(d.keys())[0]
        if np.ndim(data_processed[key[:3]]) == 1 :
            data_processed[key[:3]].append(d[key]["reward"].mean(axis=1))
            time[key[:3]] = d[key]["timesteps"]
        else :
            if np.shape(d[key]["reward"])[0] == np.shape(data_processed[key[:3]][0])[0]:
                data_processed[key[:3]].append(d[key]["reward"].mean(axis=1))
    return data_processed, time

def _plot_train(data, time, dic):
    def it2day(x):
        return x * 1e6 / (24*60*365)
    
    def day2it(x):
        return x * 24*60*365 / 1e6
    
    def _plot(mean, std, label):
        ax.plot(million_time_steps, mean, label=label)
        ax.fill_between(million_time_steps, mean-2*std, mean+2*std,
                        alpha=0.3)
        secax = ax.secondary_xaxis('bottom', functions=(it2day, day2it), color="grey")        
        secax.set_xticks(np.array([1, 3, 5, 7]))
        
    fig, ax = plt.subplots(figsize=(6,2.9), dpi=300)
    h = [plt.plot([],color="grey")[0], plt.fill_between([], [], color="grey", alpha=0.3)]
    legend = plt.legend(h,["Moyenne",r"$\pm 2 \sigma$"],loc="lower right", bbox_to_anchor=(.55, 0.12))
    ax.add_artist(legend)
    algo = ["PPO", "SAC"]
    for a in algo :
        d = np.asarray(data[a])
        train_mean = d.mean(axis=0)
        std_mean = d.std(axis=0)
        million_time_steps = time[a]/1e6
        _plot(train_mean, std_mean, a)
        
    n_steps = len(million_time_steps)
    agent_names = ["OP1", "OP2", "DSO"]
    agent_labels = ["Optimal", "Optimal avec retard", "Sans GRD"]
    for k, agent_name in enumerate(agent_names):
        mean = dic["reward_mean"][agent_name]*np.ones(n_steps)
        std = 0*np.ones(n_steps)
        _plot(mean, std, agent_labels[k])
    
    ax.legend(loc="lower right", bbox_to_anchor=(0.999, 0.02))

    ax.set_xlim((min(million_time_steps), max(million_time_steps)))
    ax.set_xlabel("Nombre d'itérations (en million / en jours simulés)")
    ax.set_ylabel("Récompense totale \n sur un épisode")
    ax.grid()
    imp.save_fig(folder, path_origin, "train")

def plot_train(path_origin, folder):
    data, dic = _import_data(path_origin, folder)
    data_processed, time = _process_data(data)
    _plot_train(data_processed, time, dic)
    
def plot_merit_order(path_origin, folder):
    env = use._import_env(path_origin, folder, vec=False)
    env.set_seed_day(50)
    env.t = int(11.2*60)
    env._plot_merit_order(ax=None, folder=folder, path_origin=path_origin, gimp=True)

def _get_data_pareto(path_origin, folder):
    """Not exactly this code, since each agent is benchmark and not evaluated""" #BUG
    path = join(path_origin, "processed/use", str(folder), "data")
    directories = [name for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))]
    out = {}
    for d in directories :
        path_to_file = join(path,d,"data/RL/eval/bench_hist.pkl")
        with open(path_to_file, 'rb') as f:
            bench_hist = pickle.load(f)
        path_to_file = join(path, d,"dict_parameters.pkl")
        with open(path_to_file, 'rb') as f:
            dic = pickle.load(f)
        out[str(d)] = {"data": bench_hist,
                       "alpha_congestion": dic["alpha_congestion"],
                       "alpha_exchange" : dic["alpha_exchange"],
                       "line_capacity" : dic["line_capacity"]}
    return out

def _process_data_pareto(data, mean=False):
    l = len(data.keys())
    alpha_congestion = np.zeros(l)
    alpha_exchange = np.zeros(l)
    agent_names = ["SAC", "PPO", "DSO", "OP1", "OP2"]
    def _init(keys):
        dic = {}
        for k in keys :
            dic[k] = []
        return  dic
    def _init_glob(keys):
        a = _init(keys)
        b = _init(keys)
        c = _init(keys)
        return a, b, c
        
    power_sum, congestion_depth, congestion_freq = _init_glob(agent_names)
    for pareto in data.keys():
        k = int(pareto)
        alpha_congestion[k] = data[pareto]["alpha_congestion"]
        alpha_exchange[k] = data[pareto]["alpha_exchange"]
        line_capa = data[pareto]["line_capacity"]
        pow_sum, cong_depth, cong_freq = _init_glob(agent_names)
        for agent in data[pareto]["data"]["info"].keys():
            if mean or (len(pow_sum[agent[:3]]) <= 3) :
                power = np.abs(np.asarray(data[pareto]["data"]["info"][agent])[:,:,1]).flatten()
                depth = np.clip(100*(power-line_capa)/line_capa,0, None)
                pow_sum[agent[:3]].append(np.mean(power))
                cong_depth[agent[:3]].append(np.mean(depth))
                cong_freq[agent[:3]].append(100*np.mean(depth>line_capa/100))
        if mean :
            for agent in agent_names :
                pow_sum[agent] = np.mean(pow_sum[agent])
                cong_depth[agent] = np.mean(cong_depth[agent])
                cong_freq[agent] = np.mean(cong_freq[agent])
        for agent in agent_names :
             power_sum[agent].append(pow_sum[agent])
             congestion_depth[agent].append(cong_depth[agent])
             congestion_freq[agent].append(cong_freq[agent])
    return power_sum, congestion_depth, congestion_freq, alpha_congestion, alpha_exchange

def _plot_pareto(power_sum, congestion_depth, congestion_freq, alpha_congestion, alpha_exchange, mean=False):
    def _key2legend(keys):
        legend = []
        for k in keys :
            if k=="DSO" : legend.append("Sans GRD")
            elif k=="OP1" : legend.append("Optimal")
            elif k=="OP2" : legend.append("Optimal avec retard")
            else : legend.append(k)
        return legend
    
    def _plot(data_x, data_y, label_x, label_y, title, size_x=30):
        fig, ax = plt.subplots(figsize=(6,2.5), dpi=300)
        marker_shape = ["o", "^", "1", "x", "+"]
        color_agent = {"DSO":"red",
                       "OP1":"red",
                       "OP2":"red",}
        for k, agent_type in enumerate(data_x.keys()):
            if (agent_type == "SAC") or (agent_type=="PPO"):    
                if mean==False :
                    for j in range(len(data_x[agent_type][0])):
                        if type(size_x) == int :
                            s = size_x
                        else :
                            s = np.asarray(size_x[agent_type])[:,j]+5
                        im = ax.scatter(np.asarray(data_x[agent_type])[:,j],
                                        np.asarray(data_y[agent_type])[:,j],
                                        s=s,
                                        marker=marker_shape[k], 
                                        c=alpha_congestion, linewidths=0.001,
                                        cmap="viridis", zorder=3)
                else :
                    if type(size_x) == int :
                        s = size_x
                    else : s = np.asarray(size_x[agent_type])+5
                    x = np.asarray(data_x[agent_type])
                    y = np.asarray(data_y[agent_type])
                    im = ax.scatter(x,y, s=s,
                                    marker=marker_shape[k], linewidths=0.001,
                                    c=alpha_congestion,
                                    cmap="viridis", zorder=3)
            else : 
                im = ax.scatter(np.asarray(data_x[agent_type])[0],
                                np.asarray(data_y[agent_type])[0],
                                marker=marker_shape[k], color=color_agent[agent_type],
                                s=20, linewidths=1, zorder=3)
        
        fig.colorbar(im, ax=ax, label=r"$\alpha$")
        color = ["grey", "grey", color_agent["DSO"], color_agent["OP1"], color_agent["OP2"]]
        h = [plt.scatter([],[], marker= marker_shape[k], color=color[k]) for k in range(len(data_x.keys()))]
        legend = plt.legend(h, _key2legend(list(data_x.keys())),loc="upper left", bbox_to_anchor=(.01,0.99))
        ax.add_artist(legend)
        
        ax.set_xlabel(label_x)
        ax.set_ylabel(label_y)
        ax.grid()
        imp.save_fig(folder, path_origin, title)
    
    _plot(power_sum, congestion_depth, "Puissance moyenne échangée (en kW)",
          "Congestion moyenne \n (en % de la capacité)", "pareto_depth")
    _plot(power_sum, congestion_freq, "Puissance moyenne échangée (en kW)",
          "Fréquence des congestions \n (en %)", "pareto_freq")#, congestion_depth)

def plot_pareto(path_origin, folder, mean=True):
    data = _get_data_pareto(path_origin, folder)
    power_sum, congestion_depth, congestion_freq, alpha_congestion, alpha_exchange = _process_data_pareto(data, mean)
    _plot_pareto(power_sum, congestion_depth, congestion_freq, alpha_congestion, alpha_exchange, mean)

def _plot_transfert(data_t, data_0, time, dic):
    def it2day(x):
        return x * 1e6 / (24*60*365)
    
    def day2it(x):
        return x * 24*60*365 / 1e6
    
    def _plot(mean, std, label, color, linestyle="solid"):
        ax.plot(million_time_steps, mean, label=label, linestyle=linestyle, color=color)
        ax.fill_between(million_time_steps, mean-2*std, mean+2*std,
                        alpha=0.3, color=color)
        secax = ax.secondary_xaxis('bottom', functions=(it2day, day2it), color="grey")
        secax.set_xticks(np.array([1, 3, 5, 7]))
        
    fig, ax = plt.subplots(figsize=(6,4), dpi=300)
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    
    # h = [plt.plot([],color="grey")[0], plt.fill_between([], [], color="grey", alpha=0.3)]
    # legend = plt.legend(h,["Moyenne",r"$\pm 2 \sigma$"],loc="lower right", bbox_to_anchor=(.27, 0.05))
    # ax.add_artist(legend)
    
    h = [plt.plot([],color=colors[k])[0] for k in [2,3,4]]
    legend = plt.legend(h,["Optimal", "Optimal avec retard", "Sans GRD"],loc="lower right", bbox_to_anchor=(.685, 0.05))
    ax.add_artist(legend)
    
    algo = ["PPO", "SAC"]
    for k, a in enumerate(algo) :
        # Plot transfered agent
        d = np.asarray(data_t[a])
        train_mean = d.max(axis=0)
        # std_mean = d.std(axis=0)
        std_mean = np.zeros(np.shape(train_mean))
        million_time_steps = time[a]/1e6
        _plot(train_mean, std_mean, a+" transfert", colors[k])
        # Plot the normal agent
        d = np.asarray(data_0[a])
        train_mean = d.max(axis=0)
        std_mean = np.zeros(np.shape(train_mean))
        _plot(train_mean, std_mean, a, colors[k], "dashed")
        
    n_steps = len(million_time_steps)
    agent_names = ["OP1", "OP2", "DSO"]
    agent_labels = ["Optimal", "Optimal avec retard", "Sans GRD"]
    for k, agent_name in enumerate(agent_names):
        mean = dic["reward_mean"][agent_name]*np.ones(n_steps)
        std = 0*np.ones(n_steps)
        _plot(mean, std, None, colors[k+2])
        
    ax.legend(loc="lower right", bbox_to_anchor=(0.999, 0.05))
    t_min = min(million_time_steps)
    t_max = max(million_time_steps)
    # Gain initial
    ax.annotate("", xy=(t_min, 10),
                xytext=(t_min, 17), size=10, arrowprops=dict(arrowstyle="<->"))
    ax.text(0.2,12.5,"Gain initial")
    # # Temps jusqu'à une performance donné
    # ax.annotate("", xy=(t_min*1.3, 30),
    #             xytext=(t_min*1.3+1.05, 30), size=10, arrowprops=dict(arrowstyle="<->"))
    # ax.text(0.2,25,"Temps jusqu'au seuil")
    # Gain asymptotique
    ax.annotate("", xy=(t_max*0.95, 32.3),
                xytext=(t_max*0.95, 42.3), size=10, arrowprops=dict(arrowstyle="<->"))
    ax.text(3.2,37.7,"Gain asymptotique")

    ax.set_xlim((min(million_time_steps), max(million_time_steps)))
    ax.set_xlabel("Nombre d'itérations (en million / en années simulées)")
    ax.set_ylabel("Récompense totale sur un épisode")
    ax.grid()
    imp.save_fig(folder, path_origin, "transfert")

def plot_transfert(path_origin, folder):
    data_transfert, dic_transfert = _import_data(path_origin, folder, n_PC=6, with_dic=True, transfer="transfer")
    data_transfert_processed, time = _process_data(data_transfert)
    data_from_scratch, dic_from_scratch = _import_data(path_origin, folder, n_PC=6, with_dic=True, transfer="from_scratch")
    data_from_scratch_processed, time = _process_data(data_from_scratch)
    _plot_transfert(data_transfert_processed,
                    data_from_scratch_processed, time, dic_from_scratch)
    # _plot_transfert(path_origin, folder, data_transfert_processed, time, dic_transfert)

def main(path_origin, folder):
    # plot_merit_order(path_origin, folder)
    # plot_train(path_origin, "data_transfert")
    plot_pareto(path_origin, "data_pareto")
    # plot_transfert(path_origin, "data_transfert")
    return 0

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Gueno_win","plots")
    data = main(folder = folder, path_origin = path_origin)