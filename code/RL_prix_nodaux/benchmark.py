#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 17:34:06 2023

@author: gcherot
"""

# Standard library
import os
from os.path import join
import numpy as np
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3 import SAC, PPO
import pickle
# Custom import
import code_these.src._import as imp
from code_these.RL_prix_nodaux.agents import Agent_no_action, Agent_nearly_opti, Agent_opti
import code_these.RL_prix_nodaux.compute as compute
import code_these.RL_prix_nodaux.use as use

def evaluate_using_stable(path_origin, folder, dic):
    agent, env = use._import_agent(path_origin, folder, Agent_nearly_opti, dic, None)
    n_episode = len(env.get_attr("dic")[0]["test_days"])
    res = evaluate_policy(agent, env, n_episode, return_episode_rewards=True)
    return res

def benchmark_agent(path_origin, folder, agent_type, dic, path_to_agent_zip=None):
    agent, env = use._import_agent(path_origin, folder, agent_type, dic, path_to_agent_zip)
    n_episode = max(12, len(env.get_attr("dic")[0]["test_days"])) #BUG dic["n_episode"]
    obs_hist, reward_hist, info = compute.evaluate(agent, n_episode, n_episode)
    return np.sum(reward_hist)/n_episode, np.std(np.sum(reward_hist, axis=1)), info

def bench_fig1(path_origin, folder):
    agent_type = [Agent_no_action, Agent_opti, Agent_nearly_opti]
    agent_name = ["DSO", "OP1", "OP2"]
    hist = {"reward_mean":{},
            "reward_std":{},
            "info":{}}
    dir_path = join(path_origin, "processed/use", str(folder), "dict_parameters.pkl")
    with open(dir_path, 'rb') as f:
        dic = pickle.load(f)
    for k, ag in enumerate(agent_type) :
        hist["reward_mean"][agent_name[k]], hist["reward_std"][agent_name[k]], hist["info"][agent_name[k]] = benchmark_agent(path_origin, folder, ag, dic)
    
    path = join(path_origin, "processed/use", str(folder), "data")
    os.makedirs(path, exist_ok=True)
    with open(join(path, 'bench_hist.pkl'), 'wb') as f:
        pickle.dump(hist, f)
        
def bench_pareto(path_origin, folder):
    """With Path to RL/checkpoint folder"""
    def _name2type(name):
        if name == "SAC": return SAC
        elif name == "PPO": return PPO
        elif name == "DSO": return Agent_no_action
        elif name == "OP1": return Agent_opti
        elif name == "OP2": return Agent_nearly_opti
        
    path_folder = join(path_origin, "processed/use", str(folder), "data")
    pareto_IDs = [name for name in os.listdir(path_folder) if os.path.isdir(os.path.join(path_folder, name))]
    for pareto_ID in pareto_IDs :
        print("Pareto ID : ", pareto_ID)
        dir_path = join(path_folder, pareto_ID, "dict_parameters.pkl")
        with open(dir_path, 'rb') as f:
            dic = pickle.load(f)
        path = join(path_folder, pareto_ID,"data/RL/checkpoint")
        agent_paths = [file for root, dirs, files in os.walk(path) for file in files]
        agent_paths = np.concatenate((agent_paths, ["DSO", "OP1", "OP2"]))
        agent_name = [name[:3] for name in agent_paths]
        agent_type = [_name2type(name) for name in agent_name]
        hist = {"reward_mean":{},
                "reward_std":{},
                "info":{}}
        for k, ag in enumerate(agent_type) :
            print(agent_paths[k])
            key = agent_name[k]+"_"+str(k)
            fun = lambda x : benchmark_agent(path_origin, folder, ag, dic, join(path,agent_paths[k]))
            # res = fun(0)
            res = use._try_vec_env(fun, n_try_max=10)
            hist["reward_mean"][key], hist["reward_std"][key], hist["info"][key] = res
        path_save = join(path, "../eval")
        os.makedirs(path_save, exist_ok=True)
        with open(join(path_save, 'bench_hist.pkl'), 'wb') as f:
            pickle.dump(hist, f)

def main(path_origin, folder):
    pass
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_electronique", "data_pareto")
    
    # bench_fig1(path_origin, folder)
    bench_pareto(path_origin, folder)
    
    # bench_pareto(path_origin, folder, 12)
    # dir_path = join(path_origin, "processed/use", str(folder), "0/data/RL/eval", 'bench_hist.pkl')
    # with open(dir_path, 'rb') as f:
    #     dic = pickle.load(f)
    # import matplotlib.pyplot as plt
    # label = ["t","power_exchanged","power_community","price_DSO","grid_price","energy_price"]
    # for k in range(6):
    #     plt.plot(dic["info"]["SAC_1"][0][:,k])
    #     plt.title(label[k])
    #     plt.show()
        
