#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 09:39:42 2023

@author: gcherot
"""
import numpy as np

def find_curve_crossings(x1, y1, x2, y2):
    """
    Find the indices where two curves cross.
    x1, y1: NumPy arrays representing the first curve.
    x2, y2: NumPy arrays representing the second curve.
    """
    # Interpolate the second curve onto the same x values as the first curve
    y2_interp = np.interp(x1, x2, y2)
    
    # Calculate the difference between the two curves
    diff = y1 - y2_interp
    
    # Find the indices where the sign of the difference changes
    crossings = np.where(np.diff(np.sign(diff)))[0]
    
    return crossings

###############################################################################
########################## INTERACTIONS AGENT / ENV ###########################
###############################################################################

from joblib import Parallel, delayed
import matplotlib.pyplot as plt


def evaluate(agent, n_episode, n_jobs = 1):
    """Evaluate the performance of an model on the testing set 'days'
    
    Parameter
    ----------
    env :
        environment to evaluate on.
    n_episode : int
        Number of episodes used to evaluate the agent performance.
    n_jobs : int
        the env is evalated in a parallel way using joblib (default is 1, no parallelization)
    """
    res = Parallel(n_jobs=n_jobs)(delayed(evaluate_episode)(agent, i) for i in range(n_episode))
    obs_hist, reward_hist, info_hist = zip(*res)
    return obs_hist, reward_hist, info_hist

def evaluate_episode(agent, epidode_number):
    """Evaluate on one episode.
    
    Parameter
    ---------
    env :
        environment to evaluate on."""
    agent.env.get_attr("env")[0].set_seed_day(epidode_number+agent.env.get_attr("dic")[0]["rd_state"])
    agent.env.get_attr("env")[0]._set_day_test(epidode_number)
    n_obs = agent.env.get_attr("n_obs")[0]
    dic = agent.env.get_attr("dic")[0]
    reward_hist = np.zeros(dic["number_of_time_step"])
    obs_hist = np.zeros((dic["number_of_time_step"], agent.env.get_attr("n_obs")[0]))
    info_hist = np.zeros((dic["number_of_time_step"], n_obs))
    
    reward_hist[0] = 0
    obs_hist[0] = obs = agent.env.reset()
    info_hist[0] = agent.env.get_attr("get_infos")[0]()
    
    done = False
    k = 1
    while done == False :
        action, _ = agent.predict(obs, deterministic=True)
        obs, rew, done, info = agent.env.step(action)
        obs_hist[k] = obs
        reward_hist[k] = rew
        info_hist[k] = agent.env.get_attr("get_infos")[0]()
        k+=1
    return obs_hist, reward_hist, info_hist

def plot_infos(agent, hist_obs=True, hist_line=True, n_episode=2, n_jobs=1):
    obs_hist, reward_hist, info_hist = evaluate(agent, n_episode, n_jobs)
    if hist_obs :
        plot_hist_obs(agent, obs_hist, reward_hist)
    if hist_line :
        plot_hist_line(agent, info_hist[0][:,1])

def plot_hist_obs(agent, obs_hist, reward_hist):
    dic = agent.env.get_attr("dic")[0]
    def _plot_histogram_reward(data):
        fig, ax = plt.subplots(dpi=100, figsize=(6,3))
        n, bins, patches = ax.hist(data, bins=100)
        ax.set_xlabel("Valeur normé")
        ax.set_ylabel("Nombre d'occurence")
        ax.set_ylim((0,None))
        ax.set_title("Histogramme récompense")
        plt.show()
     
    def _plot_histogram_obs(data):
        fig, axs = plt.subplots(data.shape[1], dpi=100, figsize=(6,2*data.shape[1]))
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        for k in range(data.shape[1]):
            ax = axs[k]
            n, bins, patches = ax.hist(data[:,k], bins=100)
            # place a text box in upper left in axes coords
            ax.text(0.05, 0.9, agent.env.get_attr("obs_name")[0][k], transform=ax.transAxes, fontsize=12,
                    verticalalignment='top', horizontalalignment="left", bbox=props)
            ax.set_yscale('symlog')
        fig.supxlabel("Valeur normée")
        fig.supylabel("Nombre d'occurence")
        fig.suptitle("Histogramme observations, rd = " + str(dic["rd_state"]))
        plt.tight_layout()
        plt.show()
            
    _plot_histogram_reward(np.asarray(reward_hist).flatten())
    obs = np.asarray(obs_hist)
    sh = obs.shape
    obs = np.reshape(obs, (sh[0]*sh[1], sh[2]))
    _plot_histogram_obs(obs)

def plot_hist_line(agent, line_hist):
    dic = agent.env.get_attr("dic")[0]
    fig, ax = plt.subplots(dpi=100, figsize=(6,3))
    n, bins, patches = ax.hist(np.abs(line_hist).flatten(), bins=100)
    ax.plot([dic["line_capacity"], dic["line_capacity"]],
            [0, 1.2*np.max(n)])
    ax.set_xlabel("Puissance transitant dans la ligne (en kW)")
    ax.set_ylabel("Nombre d'occurence")
    ax.set_ylim((0,1.2*np.max(n)))
    ax.set_title("Histogramme puissance échangée")
    plt.show()