#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 17:34:06 2023

@author: gcherot
"""
# Standard library
import os
from os.path import join
from stable_baselines3 import PPO, SAC
# Custom import
import code_these.src._import as imp
import code_these.RL_prix_nodaux.use as use
import code_these.RL_prix_nodaux.benchmark as benchmark
import code_these.src._initialize as _initialize

def _train_agent_expert(path_origin, folder, n_train = 10):
    def _fun(x):
        _initialize._set_new_seed(dic, j+100)
        train_env, eval_env = use._create_vectorised_envs(path_origin, folder, dic)
        model = agent_types[k]("MlpPolicy", train_env, verbose=1)
        model.learn(total_timesteps=total_timesteps)
        model.save(join(save_model_dir, agent_name + "_" + str(j+1)), exclude=["replay_buffer"])
        
    dic = _initialize._init_dic_RL()
    save_model_dir = join(path_origin, "processed/use/trained_agents")
    os.makedirs(save_model_dir, exist_ok=True)
    agent_names = ["SAC", "PPO"]
    agent_types = [SAC, PPO]
    for k, agent_name in enumerate(agent_names):
        for j in range(n_train):
            use._try_vec_env(_fun, n_try_max=10)
            

def _train_agent_transfert(path_origin, folder, n_train):
    def _fun(x):
        path_to_agent_zip = join(path_origin, "processed/use/trained_agents", agent_name+"_"+str(j+1)+".zip")
        train_env, eval_env = use._create_vectorised_envs(path_origin, folder, dic)
        tensorboard_log = join(path_origin, "processed/use", str(folder), "transfert/data/RL/tensorboard/")
        model = agent_types[k]("MlpPolicy", train_env, verbose=1, tensorboard_log=tensorboard_log)
        model.set_parameters(path_to_agent_zip)
        if agent_types[k] == SAC : model.replay_buffer.reset()
        callback = use._create_callbacks(path_origin, join(str(folder), "transfert"), total_timesteps,
                                         dic["n_training_envs"], eval_env, agent_name+"_"+str(j+1),
                                         n_eval=50)
        model.learn(total_timesteps=total_timesteps, callback=callback)
    
    dic = _initialize._init_dic_RL()
    _initialize._set_new_seed(dic, 0)
    _, eval_env = use._create_vectorised_envs(path_origin, folder, dic)
    # Transfert learning of two agents (SAC and PPO)
    agent_names = ["SAC", "PPO"]
    agent_types = [SAC, PPO]
    for k, agent_name in enumerate(agent_names):
        for j in range(n_train):
            use._try_vec_env(_fun, n_try_max=10)
    
def _train_agent_new(path_origin, folder, n_train):
    def _fun(x):
        model = agent_types[k]("MlpPolicy", train_env, verbose=1, tensorboard_log=tensorboard_log_dir)
        callback = use._create_callbacks(path_origin, folder, total_timesteps,
                                         dic["n_training_envs"], eval_env, agent_name+"_"+str(j+1),
                                         n_eval=50)
        model.learn(total_timesteps=total_timesteps, callback=callback)
        
    dic = _initialize._init_dic_RL()
    _initialize._set_new_seed(dic, 0)
    train_env, eval_env = use._create_vectorised_envs(path_origin, folder, dic)
    tensorboard_log_dir = join(path_origin, "processed/use", str(folder), "data/RL/tensorboard/")
    os.makedirs(tensorboard_log_dir, exist_ok=True)
    # Training on new environment
    agent_names = ["SAC", "PPO"]
    agent_types = [SAC, PPO]
    for k, agent_name in enumerate(agent_names):
        for j in range(n_train):
            use._try_vec_env(_fun, n_try_max=10)

def main(path_origin, folder, total_timesteps, n_train):
    _train_agent_expert(path_origin, folder, n_train)
    _train_agent_transfert(path_origin, folder, n_train)
    _train_agent_new(path_origin, folder, n_train)
    benchmark.bench_fig1(path_origin, folder)
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_electronique", "data_transfert")
    total_timesteps = 5_000_000
    n_train = 2
    main(path_origin, folder, total_timesteps, n_train)