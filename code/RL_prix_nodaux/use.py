#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 17:34:06 2023

@author: gcherot
"""

# Standard library
import os
from os.path import join
from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3 import PPO, SAC
from stable_baselines3.common.callbacks import CallbackList, EvalCallback, CheckpointCallback
# Custom import
import code_these.RL_prix_nodaux.environment as environment
import code_these.RL_prix_nodaux.create_test_case as ctc
import code_these.src._initialize as initialize

def _import_env(path_origin, folder, vec=True):
    dic = initialize._init_dic_RL()
    # BUG : To be remooved 
    dic = initialize._init_dic_RL()
    initialize._set_new_seed(dic, 0)
    # END
    dic = ctc.main(path_origin , folder, dic)
    dic["mode"] = "train"
    if vec :
        env = make_vec_env(environment.Env, n_envs=1, env_kwargs={"dic":dic,
                                                                  "path_origin":path_origin,
                                                                  "folder":folder})
    else :
        env = environment.Env(dic, path_origin, folder)
    return env

def make_env(path_origin, folder, dic):
    """
    """
    def _init():
        ctc.main(path_origin , folder, dic)
        env = environment.Env(dic, path_origin, folder)
        env.reset()
        return env
    return _init

def _eval_callback(path_origin, folder, total_timesteps, n_training_envs, eval_env, model_name, n_eval=50):
    eval_log_dir = join(path_origin, "processed/use", str(folder), "data/RL/eval", model_name)
    os.makedirs(eval_log_dir, exist_ok=True)
    eval_callback = EvalCallback(eval_env,
                                 best_model_save_path=eval_log_dir,
                                 log_path=eval_log_dir,
                                 eval_freq=max((total_timesteps / n_eval) // n_training_envs, 1),
                                 # eval_freq=int(max((total_timesteps / n_eval), 1)),
                                 n_eval_episodes=12,
                                 deterministic=True,
                                 render=False)
    return eval_callback

def _checkpoint_callback(path_origin, folder, total_timesteps, n_training_envs, model_name, n_eval=50):
    save_log_dir = join(path_origin, "processed/use", str(folder), "data/RL/checkpoint/")
    os.makedirs(save_log_dir, exist_ok=True)
    checkpoint_callback = CheckpointCallback(save_freq=max((total_timesteps / n_eval) // n_training_envs, 1),
                                             # save_freq=int(max((total_timesteps / n_eval), 1)),
                                             name_prefix=model_name,
                                             save_path=save_log_dir)
    return checkpoint_callback

def _create_callbacks(path_origin, folder, total_timesteps, n_training_envs, eval_env, model_name, n_eval=50):
    eval_callback = _eval_callback(path_origin, folder, total_timesteps, n_training_envs, eval_env, model_name, n_eval)
    checkpoint_callback = _checkpoint_callback(path_origin, folder, total_timesteps, n_training_envs, model_name, n_eval)
    callback = CallbackList([checkpoint_callback, eval_callback])
    return callback

def _create_vectorised_envs(path_origin, folder, dic):
    train_env = SubprocVecEnv([make_env(path_origin, folder, dic) for i in range(dic["n_training_envs"])], "spawn") # None, "spawn")                                                            "folder":folder})
    dic = ctc.main(path_origin , folder, dic)
    dic["mode"] = "test"
    eval_env = make_vec_env(environment.Env, n_envs=1, env_kwargs={"dic":dic, 
                                                                  "path_origin":path_origin,
                                                                  "folder":folder})
    return train_env, eval_env

def _try_vec_env(fun, n_try_max=20):
    n_try=0
    while n_try < n_try_max :
        try :
            res = fun(None)
            n_try = n_try_max
        except :
            print("Initialisation n°"+str(n_try)+" failed : New try")
            n_try += 1
    return res
            
def _import_agent(path_origin, folder, agent_type, dic, path_to_agent_zip=None):
    _, env = _create_vectorised_envs(path_origin , folder, dic)
    if agent_type in [PPO, SAC]:
        agent = agent_type.load(path_to_agent_zip, env)
    else :
        agent = agent_type(env)
    return agent, env