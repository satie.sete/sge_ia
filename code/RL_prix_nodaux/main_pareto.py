#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 17:34:06 2023

@author: gcherot
"""

# Standard library
import os
from os.path import join
import numpy as np
from stable_baselines3 import SAC, PPO
# Custom import
import code_these.src._import as imp
import code_these.RL_prix_nodaux.use as use
import code_these.RL_prix_nodaux.benchmark as benchmark
import code_these.src._initialize as _initialize

def train(path_origin, folder, n_train, total_timesteps, k, n_pareto):
    def setup():
        dic = _initialize._init_dic_RL()
        dic["alpha_congestion"] = np.linspace(0,1,n_pareto)[k]
        dic["alpha_exchange"] = 1-dic["alpha_congestion"]
        n_envs= 12
        train_env, eval_env = use._create_vectorised_envs(path_origin, join(str(folder),str(k)), dic)
        save_log_dir = join(path_origin, "processed/use", str(folder), str(k), "data/RL/checkpoint/")
        os.makedirs(save_log_dir, exist_ok=True)
        return train_env, save_log_dir, n_envs
        
    def para(train_env, save_log_dir, j):
        agent = [SAC, PPO]
        agent_names = ["SAC", "PPO"]
        for k, ag in enumerate(agent) :
            model = ag("MlpPolicy", train_env, verbose=1)
            model_to_save = model.learn(total_timesteps=total_timesteps)
            model_to_save.save(join(save_log_dir, agent_names[k]+"_"+str(j)))
    
    train_env, save_log_dir, n_envs = setup()
    for j in range(n_train):
        para(train_env, save_log_dir, j)

def main(path_origin, folder, n_train, total_timesteps, n_pareto, PC_ID=None):
    """Use PC_ID only for distributed training"""
    if PC_ID == None :
        for k in range(n_pareto):
            train(path_origin, folder, n_train, total_timesteps, k, n_pareto)
    else :
        train(path_origin, folder, n_train, total_timesteps, PC_ID, n_pareto)
        # train(path_origin, folder, n_train, total_timesteps, PC_ID+6, n_pareto)
    benchmark.bench_pareto(path_origin, folder, 12)
    
if __name__ == "__main__":
    PC_ID = 0 # Each computer will train on a specific alpha ratio (If one PC fails, simulation as to be computed again for that computer ID)
    path_origin, folder = imp.path_import("PC_electronique", "data_pareto")
    n_train = 5
    n_pareto = 6
    total_timesteps = 5_000_000
    # main(path_origin, folder, n_train, total_timesteps, n_pareto, PC_ID)
    fun = lambda x : main(path_origin, folder, n_train, total_timesteps, n_pareto, PC_ID)
    use._try_vec_env(fun, n_try_max=20)