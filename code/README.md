# Installation des librairies

- Le calcul de l'OPF nécessite la communication entre PowerModels (Julia) et PandaPower (python). Pour les versions permettant ce dialogue, il est impossible de récupérer les variables duales issues de l'optimisation.
Nous proposons donc l'extension suivante : [GitHub](https://github.com/gcherot/Dual_variables_PowerModels)