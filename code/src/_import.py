#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes
"""
import os
from os.path import join
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt

def import_tables(path_origin):
    """Import info of time series.
    Parameter 
    ---------
    - path_origin : str
        Path where data is stored (both times series and vreated test case). By default in the NASS.
    Return 
    ---------
    apt_table, pv_table, ev_table"""
    path_folder = os.path.join(path_origin, "processed/data/")
    # Import id from pv, apt et ev
    apt_info = pd.read_csv(os.path.join(path_folder, r"apt/info.csv"), delimiter=',')
    pv_info = pd.read_csv(os.path.join(path_folder, r"pv/info.csv"), delimiter=',')
    ev_info = pd.read_csv(os.path.join(path_folder, r"ev/info.csv"), delimiter=',')
    return apt_info, pv_info, ev_info

def path_import(device,folder):
    """Import the path in wich data is stored.
    Parameter
    ---------
    - device : str
        ["NAS_Gueno_win", "NAS_Bertha", "PC_Gueno_win", "PC_Gueno_ubuntu", "PC_Bertha", "PC_Roman"]
    - folder : int
        Id of the folder where testcase is saved.
        
    Return
    ---------
    - path_origin : str
        Path where data is stored (both times series and vreated test case).
    """
    path_origin_dict   = {"NAS_Gueno_win" : "h:/data", 
                         "NAS_Bertha" : "/run/user/1004/gvfs/smb-share:domain=ensr-win.local,server=srv-nas01.ensr-win.local,share=home,user=gcher495/gcher495/data",
                         "PC_Gueno_win" : "C:/Users/Guénolé/Documents/These/Science/data",
                         "PC_electronique" : "C:/Users/gcher495/Documents/data",
                         "PC_Gueno_ubuntu" : "/home/cherot/Documents/data",
                         "PC_Bertha" : "",
                         "PC_Fabien" : "C:/Users/fabie/Desktop/4A/data",
                         "PC_Roman" : "/home/gcherot/Documents/data"}
    path_origin = path_origin_dict[device]
    return path_origin, folder

def get_directories_path(dir_path):
    """Returns all subdirectory in the 'dir_path' directory."""
    path_list = [x for x in os.walk(dir_path)] # Get all file name
    return path_list[0][1] # Directories are in the second column

def get_undone_path(dir_path):
    """Returns all subdirectory in the 'dir_path' directory."""
    path_list = [x for x in os.walk(dir_path)] # Get all file name
    out = []
    for d in path_list[0][1] :
        for x in os.walk(join(dir_path, d)) :
            if len(x[1]) == 2 :
                out.append(d)
    return out

def get_files_path(dir_path, typ="csv"):
    """Returns all files in the 'dir_path' directory.
    Parameter
    ---------
    - typ : str
        Type of the file to look for. (default is 'csv')
    """
    path_list = [x for x in os.walk(dir_path)] # Get all file name
    lis = []
    try :
        for x in path_list[0][2] :
            if x.endswith(typ) :
                lis.append(x)
        return lis
    except : return []

def setup_folders_data(save_path):
    os.makedirs(save_path, exist_ok=True)
    names = ["dual",
             "res_bus",
             "res_line",
             "res_trafo",
             "res_ext_grid",
             "res_load",
             "res_gen",
             "res_welfare"]
    for name in names :
        path = join(save_path,name)
        os.makedirs(path, exist_ok=True)
    return names

def read_all_variables(folder, path_origin, folder_name):
    variables = []
    var_names = ["dual",
                 "res_bus",
                 "res_line",
                 "res_trafo",
                 "res_ext_grid",
                 "res_load",
                 "res_gen",
                 "res_welfare"]
    for var in var_names :
        var, days = _read_variable(folder, path_origin, join(folder_name,var))
        variables.append(var)
    return variables, var_names, days

def _read_variable(folder, path_origin, var):
    dir_path = join(path_origin, "processed/use", str(folder), "data", var)
    files = get_files_path(dir_path, "csv") + get_files_path(dir_path, "npy")
    try :
        temp_file = _load(join(dir_path, files[0]))
    except :
        return []
    sh, time_step = _shape(temp_file, files)
    variable = []
    for k in range(len(files)):
        path = join(dir_path, files[k])
        begin = k*time_step
        end = (k+1)*time_step
        variable.append(_load(path))
    variable = np.concatenate(variable)
    # variable = np.delete(variable, np.where(np.sum(variable,axis=1) == 0), axis=0) # BUG Delete days for wich all duals are nulls
    return variable, files

def load_dict(folder, path_origin, file_name):
    dir_path = join(path_origin, "processed/use", str(folder), "data", file_name+".pkl")
    with open(dir_path, 'rb') as f:
        loaded_dict = pickle.load(f)
    return loaded_dict

def load_time(folder, path_origin):
    file = join(path_origin, "processed/use", str(folder), "data", "time.csv")
    return _load(file, typ="csv")

def _load(file, typ="csv"):
    if file[-3:] == typ:
        return np.loadtxt(file, delimiter=",", dtype=float)
    else :
        return np.load(file)
    
def _shape(array, files):
    if len(array.shape) == 2:
        return (len(files)*array.shape[0],array.shape[1]), array.shape[0]
    if len(array.shape) == 3:
        return (len(files)*array.shape[0],array.shape[1], array.shape[2]), array.shape[0]
    else :
        return len(files)*array.shape[0], array.shape[0]
    
def save_model(save_path, model_name, model):
    pkl_filename = join(save_path, model_name + ".pkl")
    os.makedirs(save_path, exist_ok=True)
    with open(pkl_filename, 'wb') as file:
        pickle.dump(model, file)
        
def load_model(save_path, model_name):
    pkl_filename = join(save_path, model_name + ".pkl")
    os.makedirs(save_path, exist_ok=True)
    with open(pkl_filename, 'rb') as file:
        pickle_model = pickle.load(file)
    return pickle_model

def save_fig(folder, path_origin, title, ext=".pdf"):
    # SAVE
    dir_path = join(path_origin, "processed/use", str(folder), "plot")
    os.makedirs(dir_path, exist_ok=True)
    plt.savefig(join(dir_path, title+ext), bbox_inches="tight")
    print(join(dir_path, title+ext))
    plt.close()

#################################################################################
##################################### POWER #####################################
#################################################################################

def _import_power(agent_table, k, data_path, day):
    id_data = int(agent_table.loc[k, "id_data"])
    typ = agent_table.loc[k, "type"]
    path = join(data_path, typ, str(id_data), str(day)+".csv")
    scale=1
    return scale*np.maximum(np.loadtxt(path, delimiter=","),0)

def import_power_as_dict(agent_table, day, data_path):
    """Import agent power"""
    data = {}
    for k in agent_table.index:
        data[k] = _import_power(agent_table, k, data_path, day)
    return data

def import_power_as_dataframe(agent_table, day, data_path):
    """Import agent power"""
    data = pd.DataFrame(columns = np.arange(0,24*60).astype(str), index=agent_table.index)
    for k in agent_table.index:
        data.iloc[k] = _import_power(agent_table, k, data_path, day)
    return data

def import_power_as_array(agent_table, day, data_path):
    """Import agent power"""
    data = np.zeros((len(agent_table.index), 24*60))
    for k in agent_table.index:
        data[k] = _import_power(agent_table, k, data_path, day)
    return data

def import_epex(day, data_path, year=2019):
    """Import agent power"""
    path = join(data_path, "EPEX", str(year), str(day)+".csv")
    data = np.loadtxt(path, delimiter=",")
    return data













































