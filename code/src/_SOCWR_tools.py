# -*- coding: utf-8 -*-
"""
Created on Thu Sep  8 14:25:52 2022

@author: Guenole

JULIA NOTATIONS

p[(i, j, k)] :
    i : id of the line
    j : bus from
    k : bus to
pg[i] : power injected by agent i
w[i] : voltage squared on bus i
wi[k,l] : Im(Vk Vl)
wr[k,l] : Re(Vk Vl)
"""
import numpy as np
import re

def get_all_dual(data):
    """Ranked in the same order as in https://lanl-ansi.github.io/PowerModels.jl/stable/math-model/#AC-Optimal-Power-Flow-for-the-Branch-Flow-Model"""
    
    link_relaxation = get_relaxation(data) # Voltage production relaxation
    # slack bus (angle = 0) (not in model ?)
    bus_injection_p = get_injection_p(data) # active power generator/load power limits
    bus_injection_q = get_injection_q(data) # reactive power generator/load power limits
    bus_voltage_magnitude = get_voltage_magnitude(data) # Bus Voltage magnitude limits (squared)
    bus_nodal_p = get_nodal_price_p(data) # Bus active power balance
    bus_nodal_q = get_nodal_price_q(data) # Bus reactive power balance
    link_ohm_law = get_ohm(data) # Power loss on a branch (physics)
    # Compute voltage (physics) (not explicit in this model)
    # Current flow due to voltage difference (not explicit in this model)
    link_thermal = get_thermal(data) # thermal limits
    # Current limits (not in model ? nearly equivalent to thermal + Voltage)
    link_voltage_angle = get_angle(data) # Voltage angle difference : wr.jl (line 513)
    
    # Not found yet what it is
    bus_link_power_p = get_line_power_p(data) # Line active power limits at bus
    bus_link_power_q = get_line_power_q(data) # Line reactive power limits at bus
    bus_wi_wr = get_wi_wr(data)
    
    dict_out = {
        "active power balance" : bus_nodal_p, 
        "reactive power balance" : bus_nodal_q,
        "bus voltage magnitude" : bus_voltage_magnitude, 
        "apparent power flow" : link_thermal,
        "SOC relaxation" : link_relaxation, 
        "ViVj limits" : bus_wi_wr,
        "agent active power limit" : bus_injection_p, 
        "agent reactive power limit" : bus_injection_q,
        "ohm law" : link_ohm_law,
        "voltage angle" : link_voltage_angle,
        "active power flow" : bus_link_power_p, 
        "reactive power flow" : bus_link_power_q}
    _change_index(dict_out) # Change index to be consistent with python indexing (and not Julia)
    return dict_out

def organize_dual(dual, id_dual):
    # Show dual values separated by class
    dict_out = {}
    for name in list(id_dual.keys()): 
        dict_out[name] = dual[id_dual[name]["id_dual"]]
    return dict_out

##############################################################################
##############################################################################

def get_nodal_price_p(data):
    """Constrain on the active power injected at each bus"""
    bus = _get_nodal(data, "p")
    return bus

def get_nodal_price_q(data):
    """Constrain on the reactive power injected at each bus"""
    bus = _get_nodal(data, "q")
    return bus


def get_relaxation(data):
    """contrains on |W|² <= WiiWjj (Second order cone relaxation)
    
    Take dual DataFrame from PowerModels.
    Return 2D array :
        - Id of the raw containing a bus egality constrain
        - Id of the bus (PowerModels convention (strats at 1))
        """
    id_dual = []
    id_from = []
    id_to = []
    for k in range(len(data)):
        if data.Name.loc[k].find("wr") != -1 :
            if data.Name.loc[k].find("²") != -1 :
                id_dual.append(k)
                begin = data.Name.loc[k].find("wr")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])
                id_from.append(int(numbers[0]))
                id_to.append(int(numbers[1]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to}
    return dict_out

def get_injection_p(data):
    """Constrain on the active power injected by each agent"""
    bus = _get_injection(data, "p")
    return bus

def get_injection_q(data):
    """Constrain on the active power injected by each agent"""
    bus = _get_injection(data, "q")
    return bus

def get_voltage_magnitude(data):
    """Contrain on the voltage magnitude on each bus"""
    id_dual = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("wr") == -1 :
            if data.Name.loc[k].find("w[") != -1 :
                id_dual.append(k)
                
                begin = data.Name.loc[k].find("w[")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])
                id_bus.append(int(numbers[0]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus" : id_bus}
    return dict_out

def find_number_in_str(string):
    emp_lis = []
    for z in string.split():
        if z.isdigit():
            emp_lis.append(int(z))
    return emp_lis

def get_ohm(data):
    """Contrain on the voltage magnitude on each bus"""
    id_dual = []
    id_from = []
    id_to = []
    for k in range(len(data)):
        if data.Name.loc[k].find("p[(") != -1 or data.Name.loc[k].find("q[(") != -1:
            if data.Name.loc[k].find("w[") != -1 :
                id_dual.append(k)
                
                begin = data.Name.loc[k].find("wr[")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])              
                id_from.append(int(numbers[0]))
                id_to.append(int(numbers[1]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to}
    return dict_out

def get_thermal(data):
    """Constrain on apparent power going trough each line.
    
    Take dual DataFrame from PowerModels.
    Return 2D array :
        - Id of the raw containing a bus egality constrain
        - Id of the bus (PowerModels convention (starts at 1))
        """
    id_dual = []
    id_from = []
    id_to = []
    id_line = []
    for k in range(len(data)):
        if data.Name.loc[k].find("p[(") != -1 :
            if data.Name.loc[k].find("q[(") != -1 :
                id_dual.append(k)
                
                begin = data.Name.loc[k].find("p[(")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])             
                id_line.append(int(numbers[0]))
                id_from.append(int(numbers[1]))
                id_to.append(int(numbers[2]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to,
        "id_line" : id_line}
    return dict_out

def get_line_power_p(data):
    bus = _get_line_power(data, "p")
    return bus

def get_line_power_q(data):
    bus = _get_line_power(data, "q")
    return bus

def get_angle(data):
    link_1 = _get_angle_1(data)
    link_2 = _get_angle_2(data)
    return _concat_dict(link_1, link_2)

def get_wi_wr(data):
    bus_1 = _get_wi(data)
    bus_2 = _get_wr(data)
    return _concat_dict(bus_1, bus_2)

###############################################################
#################### To use internaly only ####################
###############################################################

def _concat_dict(dict_1, dict_2):
    dict_out = {}
    for name, values in dict_1.items():
        dict_out[name] = np.concatenate((values, dict_2[name]))
    return dict_out

def _get_thermal_2(data):
    """Constrain on the active/reactive power going trough each line
    
    Take dual DataFrame from PowerModels.
    Return 2D array :
        - Id of the raw containing a bus egality constrain
        - Id of the bus (PowerModels convention (starts at 1))
        """
    id_dual = []
    id_from = []
    id_to = []
    for k in range(len(data)):
        found = False
        if data.Name.loc[k].find("w") == -1 :
            if data.Name.loc[k].find("g") == -1 :
                if data.Name.loc[k].find("p[(") != -1 :
                    if data.Name.loc[k].find("q[(") == -1 :
                        found = True
                        marker = 'p'
                if data.Name.loc[k].find("q[(") != -1 :
                    if data.Name.loc[k].find("p[(") == -1 :
                        found = True
                        marker = 'q'
        if found :
            id_dual.append(k)
            begin = data.Name.loc[k].find(marker+"[(")
            numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])             
            id_from.append(int(numbers[1]))
            id_to.append(int(numbers[2]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to}
    return dict_out

def _get_wi(data):
    id_dual = []
    id_from = []
    id_to = []
    for k in range(len(data)):
        if data.Name.loc[k].find("wi") != -1 :
            if data.Name.loc[k].find("w[") == -1 and data.Name.loc[k].find("wr") == -1 and data.Name.loc[k].find("p") == -1:
                id_dual.append(k)
                begin = data.Name.loc[k].find("wi")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])
                id_from.append(int(numbers[0]))
                id_to.append(int(numbers[1]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to}
    return dict_out

def _get_wr(data):
    id_dual = []
    id_from = []
    id_to = []
    for k in range(len(data)):
        if data.Name.loc[k].find("wr") != -1 :
            if data.Name.loc[k].find("w[") == -1 and data.Name.loc[k].find("wi") == -1 and data.Name.loc[k].find("p") == -1:
                id_dual.append(k)
                begin = data.Name.loc[k].find("wr")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])
                id_from.append(int(numbers[0]))
                id_to.append(int(numbers[1]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to}
    return dict_out

def _get_angle_1(data):
    id_dual = []
    id_from = []
    id_to = []
    for k in range(len(data)):
        if data.Name.loc[k].find("wr") != -1 and data.Name.loc[k].find("wi") != -1 :
            if data.Name.loc[k].find("q") == -1 and data.Name.loc[k].find("p") == -1 and data.Name.loc[k].find("²") == -1:
                id_dual.append(k)
                
                begin = data.Name.loc[k].find("wr[")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])              
                id_from.append(int(numbers[0]))
                id_to.append(int(numbers[1]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to}
    return dict_out

def _get_angle_2(data):
    id_dual = []
    id_from = []
    id_to = []
    for k in range(len(data)):
        if data.Name.loc[k].find("wr") != -1 and data.Name.loc[k].find("w[") != -1 and data.Name.loc[k].find(">=") != -1 :
            id_dual.append(k)
            
            begin = data.Name.loc[k].find("wr[")
            numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])             
            id_from.append(int(numbers[0]))
            id_to.append(int(numbers[1]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_from,
        "id_bus_to" : id_to}
    return dict_out

def _get_line_power(data, p_or_q):
    id_dual = []
    id_bus_from = []
    id_bus_to = []
    id_line = []
    for k in range(len(data)):
        if data.Name.loc[k].find("_"+p_or_q+"[(") != -1 :
            if data.Name.loc[k].find("w") == -1 and data.Name.loc[k].find("²") == -1 and data.Name.loc[k].find("g") == -1 and data.Name.loc[k].find("==") == -1:
                id_dual.append(k)
                
                begin = data.Name.loc[k].find("_"+p_or_q+"[(")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])                                  
                id_line.append(int(numbers[0]))
                id_bus_from.append(int(numbers[1]))
                id_bus_to.append(int(numbers[2]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus_from" : id_bus_from,
        "id_bus_to" : id_bus_to,
        "id_line" : id_line}
    return dict_out

def _get_injection(data, p_or_q):
    """Constrain on the active/reactive power injected by each agent"""
    id_dual = []
    id_agent = []
    for k in range(len(data)):
        if data.Name.loc[k].find("_"+p_or_q+"[(") == -1 :
            if data.Name.loc[k].find(p_or_q+"g") != -1 :
                id_dual.append(k)
                begin = data.Name.loc[k].find(p_or_q+"g")
                numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])
                id_agent.append(int(numbers[0]))
    dict_out = {
        "id_dual" : id_dual,
        "id_agent" : id_agent}
    return dict_out

def _get_nodal(data, p_or_q):
    """Constrain on the active/reactive power injected at each bus.
    
    Take dual DataFrame from PowerModels.
    Return 2D array :
        - Id of the raw containing a bus egality constrain
        - Id of the bus (PowerModels convention (strats at 1))
        """
    id_dual = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("w") == -1 :
            if data.Name.loc[k].find("<") == -1 and data.Name.loc[k].find(">") == -1:
                if data.Name.loc[k].find("=") != -1 : # BUG : use to be "=="
                    if data.Name.loc[k].find("_"+p_or_q+"[(") != -1 :
                        id_dual.append(k)
                        
                        begin = data.Name.loc[k].find("_"+p_or_q+"[(")
                        numbers = re.findall('[0-9]+', data.Name.loc[k][begin:])
                        id_bus.append(int(numbers[1]))
    dict_out = {
        "id_dual" : id_dual,
        "id_bus" : id_bus}
    return dict_out

###################################################################################################
######################################## HELPER FUNCTIONS #########################################
###################################################################################################

def _create_dual_array_sorted(data):
    dict_out = get_all_dual(data)
    dual = {}
    for k in dict_out:
        dual[k] = data.loc[dict_out[k]["id_dual"]]
    dual_not_considered = _dual_not_considered(data)
    dual["not considered"] = data.loc[dual_not_considered]
    return dual

def _dual_not_considered(data):
    array = np .arange(0,len(data),1)
    dual_considered = _unique_dual(data)
    dual_not_considered = np.delete(array, dual_considered)
    return dual_not_considered

def _unique_dual(data):
    dict_out = get_all_dual(data)
    array = []
    for k in dict_out:
        array.append(dict_out[k]["id_dual"])
    unique = np.unique(np.concatenate(array))
    return unique

def _change_index(dict_out):
    for k in dict_out :
        dict_out[k].keys()
        for i, j in enumerate(dict_out[k]):
            if i == 0 : pass
            else :
                dict_out[k][j] = [i - 1 for i in dict_out[k][j]] # indexes in julia start by 1 and python by 0
    return dict_out

###################################################################################################
############################################## TEST ###############################################
###################################################################################################

def _test_missing_dual(data):
    dict_out = get_all_dual(data)
    n_dual = len(data)
    n_dual_dict = 0
    for k in dict_out:
        n_dual_dict += len(dict_out[k]["id_dual"])
    dup = _duplicate_dual(data)
    assert n_dual == n_dual_dict # No missing duals
    assert len(dup) == 0 # No duplicates

def _duplicate_dual(data):
    dict_out = get_all_dual(data)
    array = []
    for k in dict_out:
        array.append(dict_out[k]["id_dual"])
    u, c = np.unique(np.concatenate(array), return_counts=True)
    dup = u[c > 1]
    return data.loc[dup]
