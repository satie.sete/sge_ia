# -*- coding: utf-8 -*-
"""
Created on Thu Mar 16 15:17:31 2023

@author: Guenole
"""
# Custom import
import code_these.src._initialize as initialize
import code_these.src._import as imp
from code_these.dimension_reduction.generate_data import save_dict_parameters

def _modify(dic_to_modify, dic):
    for k in dic.keys() :
        if k not in  dic_to_modify.keys() :
            print(k, " : ADDED TO DIC")
            dic_to_modify[k] = dic[k]
    return dic_to_modify

def main(path_origin, folder):
    dic = initialize._init_dic()
    dic_to_modify = imp.load_dict(folder, path_origin, "../dict_parameters")
    dic_to_modify = _modify(dic_to_modify, dic)
    save_dict_parameters(path_origin, folder, dic_to_modify)

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman", 2)
    main(path_origin, folder)