# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 10:32:33 2022

@author: Guenole
"""
# Standard libraries
import numpy as np

def _test_SOC_relaxation(net, id_dual):
    """Test if constraints associated whith SOC relaxation are active.
    If yes, dual variable should not be null.
    
    returns 1 if test ok, 0 if test failed."""
    dual_SOC = net.dual.dual_value[id_dual["SOC relaxation"]["id_dual"]]
    min_dual_SOC = np.min(np.abs(dual_SOC))
    if min_dual_SOC < 1e-2 : res = 0
    else : res =1
    return res, min_dual_SOC

def _test_congestion(net, id_dual):
    """Test if there is a congestion in the network.
    
    returns 1 if congestion, 0 if not."""
    congestion = net.res_line["loading_percent"]/net.line["max_loading_percent"]
    if np.max(congestion) > 1 : res = 1
    else : res = 0
    return res, np.max(congestion)

def _test_LMP_congestion(net, id_dual):
    """Test if all LMP are the same when their is no congestion.
    
    returns :
    Result :
        - 1 if test ok
        - 0 if test failed
    Info
        - 1 if LMP unique and not congestion.
        - 2 if LMP different and congestion
        - 3 if LMP different and no congestion
        - 4 if LMP unique and congestion"""
    dual_LMP = net.dual.dual_value[id_dual["active power balance"]["id_dual"]].values
    dual_LMP_repeat = np.repeat(dual_LMP.reshape((1,dual_LMP.size)), dual_LMP.size, axis = 0)
    distance = np.abs(dual_LMP_repeat - dual_LMP_repeat.T)
    relative_max_distance = np.max(distance)/np.max(np.abs(dual_LMP))
    info, max_congestion = _test_congestion(net, id_dual)
    if relative_max_distance < 0.01 and info == 0 : info, res = (1,1)
    elif relative_max_distance > 0.01 and info == 1 : info, res = (2,1)
    elif relative_max_distance > 0.01 and info == 0 : info, res = (3,0)
    elif relative_max_distance < 0.01 and info == 1 : info, res = (4,0)
    return res, info, relative_max_distance, max_congestion

