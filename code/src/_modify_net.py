#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021


Implement the function to fast import an environnement.
Imported through this mean respect the gym interface.
A RL can be set to learn how to maximize the reward.
"""
import numpy as np
import pandapower as pp
import pandas as pd
from copy import deepcopy

############################################################################
################################### INIT ###################################
############################################################################

def init_load_gen(net, all_feasible=False):
    """Initialize all loads."""
    net.load = net.load[0:0] # delete old load
    net.gen = net.gen[0:0] # delete old gen
    _init_agents(net, all_feasible)
    return net

def _init_agents(net, all_feasible=False):
    """Initialize all loads."""
    id_network = np.sort(net.agent_table["id_network"].unique())
    net.number_agent = int(max(id_network))
    n_flexible = np.max(id_network) # TODO : have to be change in order to have non flexible agents
    # replace by new ones, from the time serie
    for k in id_network:
        agent = net.agent_table[net.agent_table["id_network"]==k].iloc[0]
        if k > n_flexible:
            controllable = False
        else :
            controllable = True
        if agent.et == "load":
            pp.create_load(net,
                           name=str(k),
                           index = k,
                           bus = int(agent.bus),
                           p_mw = 0,
                           controllable = controllable)
            net.load.loc[k,["type_agent", "infinite_price"]] = [agent.type, False]
            if all_feasible :
                pp.create_load(net,
                               name=str(k+net.number_agent),
                               index = k+net.number_agent,
                               bus = int(agent.bus),
                               p_mw = 0,
                               controllable = controllable)
                net.load.loc[k+net.number_agent,["type_agent", "infinite_price"]] = [agent.type, True]
        elif agent.et == "gen":
            pp.create_gen(net,
                          name=str(k),
                           index = k,
                           bus = int(agent.bus),
                           p_mw = 0)
            net.gen.loc[k,"type_agent"] = agent.type
    return net

def init_poly_cost(net):
    """Initialize the polycost in panda power network"""
    # Number of flexible load
    net.poly_cost = net.poly_cost[0:0] # Set the right number of polycost
    df_0 = _init_poly(net, "ext_grid")
    df_1 = _init_poly(net, "load")
    df_2 = _init_poly(net, "gen")
    net.poly_cost = pd.concat([df_0, df_1, df_2], ignore_index=False)
    net.poly_cost[["cq0_eur", "cq1_eur_per_mvar", "cq2_eur_per_mvar2"]] = [0, 0, 0]
    net.poly_cost[['cp0_eur', 'cp1_eur_per_mw', 'cp2_eur_per_mw2']] = [0, 0, 0]
    return net

def _init_poly(net, et):
    df = net.poly_cost[0:0]
    if et == "load" :
        df.loc[:,"element"] = net.load.index
        df.loc[:,"et"] = "load"
    elif et == "gen" :
        df.loc[:,"element"] = net.gen.index
        df.loc[:,"et"] = "gen"
    elif et == "ext_grid" :
        df.loc[:,"element"] = net.ext_grid.index
        df.loc[:,"et"] = "ext_grid"
    df = df.set_index('element', drop = False)
    return df

def init_network_constrains(net):
    """Network constrains : bus, lines, trafos"""
    # Bus constrains
    net.bus["min_vm_pu"] = 0.9
    net.bus["max_vm_pu"] = 1.1
    # Line constrains
    net.line["max_loading_percent"] = 100
    # net.line["max_i_ka"] = ? # Should be set in the network definition
    # Trafo constrains
    net.trafo["max_loading_percent"] = 100
    return net

def init_operational_constrains(net):
    """Load and generator constrains : min/max power produced"""
    # load constrains
    net.load[["q_mvar", "min_q_mvar","max_q_mvar","p_mw","min_p_mw","max_p_mw"]] = [0,0,0,0,0,0]
    net.gen[["q_mvar", "min_q_mvar","max_q_mvar","p_mw","min_p_mw","max_p_mw"]] = [0,0,0,0,0,0]
    return net

def init_net(net, all_feasible=False):
    net = init_load_gen(net, all_feasible)
    net = init_poly_cost(net)
    net = init_network_constrains(net)
    net = init_operational_constrains(net)
    return net

############################################################################
################################### SET ####################################
############################################################################

def set_load_to_ward(net, net_tilde):
    net.buffer_load = deepcopy(net.load)
    for id_load in net.load.index :
        vm_pu = net_tilde.res_bus.loc[net_tilde.load.loc[id_load, "bus"], "vm_pu"]
        pp.create_ward(net,
                       index = id_load,
                       bus=net.load.loc[id_load, "bus"],
                       ps_mw=0,
                       qs_mvar=0,
                       pz_mw=net.load.loc[id_load, "p_mw"]/(vm_pu**2),
                       qz_mvar=net.load.loc[id_load, "q_mvar"]/(vm_pu**2))
        pp.drop_elements_simple(net, "load", id_load)

def set_ward_to_load(net):
    for id_load in net.buffer_load.index :
        pp.create_load(net,
                       bus = net.buffer_load.loc[id_load, "bus"],
                       index = id_load,
                       p_mw = net.buffer_load.loc[id_load, "p_mw"],
                       min_p_mw = net.buffer_load.loc[id_load, "min_p_mw"],
                       max_p_mw = net.buffer_load.loc[id_load, "max_p_mw"],
                       q_mvar = net.buffer_load.loc[id_load, "q_mvar"],
                       min_q_mvar = net.buffer_load.loc[id_load, "min_q_mvar"],
                       max_q_mvar = net.buffer_load.loc[id_load, "max_q_mvar"],
                       controllable = net.buffer_load.loc[id_load, "controllable"],
                       )
    for id_ward in net.ward.index :
        pp.drop_elements_simple(net, "ward", id_ward)
    
def set_res_ward_to_res_load(net):
    for id_ward in net.res_ward.index :
        net.res_load.loc[id_ward] = net.res_ward.loc[id_ward]

def set_poly_cost(net, agent_cp1, agent_cp2):
    net.poly_cost["cp1_eur_per_mw"] = agent_cp1 # Set the price as wanted in import_price
    net.poly_cost["cp2_eur_per_mw2"] = np.abs(agent_cp2)*np.sign(np.sign(agent_cp1)+.5) # Trick to consider 0 positive.
    return net

def set_bounds_agents(net, power_agent):
    """Set power limits for gen and load."""
    if net.allow_q :
        min_q_mvar = -1
        max_q_mvar = 1
    else : min_q_mvar = max_q_mvar = 0
    for ind_agent in power_agent.index[2:]:
        et = net.agent_table.loc[net.agent_table["id_network"]==ind_agent, "et"].values[0]
        if et == "load":
            # The agent flexibility is in [0.9, 1.5] * P. It is decomposed in two agents
            # The first as a very high price, will consume in almost every cases (bounded in [0, 0.9]*P)
            net.load.loc[ind_agent+net.number_agent, ["p_mw", "min_p_mw", "max_p_mw"]] = power_agent[ind_agent]*np.array([0.9, 0, 0.9])
            # The second as a normal price, will consume the rest of the power (bounded in [0, 0.6]*P) => Sum in [0.9, 1.5] with a part more flexible
            net.load.loc[ind_agent, ["p_mw", "min_p_mw", "max_p_mw"]] = power_agent[ind_agent]*np.array([0.1, 0, 0.6])
            net.load.loc[ind_agent, ["q_mvar", "min_q_mvar","max_q_mvar"]] = 1.6*np.abs(power_agent[ind_agent])*np.array([0, min_q_mvar, max_q_mvar])
        elif et == "gen":
            
            net.gen.loc[ind_agent, ["p_mw", "max_p_mw", "min_q_mvar","max_q_mvar"]] = power_agent[ind_agent]*np.array([1, 1, min_q_mvar, max_q_mvar]) # would have been better to set a constrain on apparent power but no possible in pandapower        

def _bound_less_strict(df):
    neg_pos = np.asarray([[1.001, 0.999], [0.999, 1.001]])
    out = np.zeros((len(df), 4))
    sign = ((np.sign(df.loc[:, ["min_p_mw", "min_q_mvar"]])+1)/2).astype(int)
    out[:, 0:2] = neg_pos[sign.loc[:,"min_p_mw"]]
    out[:, 2:] = neg_pos[sign.loc[:,"min_q_mvar"]]
    return out
        

def set_power(net, res_load, res_gen, res_ext_grid=None, consider_ext_grid=False):
    net.gen.loc[:,"p_mw"] = net.gen.loc[:,"min_p_mw"] = net.gen.loc[:,"max_p_mw"] = res_gen.loc[:,"p_mw"]
    net.gen.loc[:,"q_mvar"] = net.gen.loc[:,"min_q_mvar"] = net.gen.loc[:,"max_q_mvar"] = res_gen.loc[:,"q_mvar"]
    mult = _bound_less_strict(net.gen)
    net.gen.loc[:, ["min_p_mw", "max_p_mw", "min_q_mvar","max_q_mvar"]] *= mult #Bound less strict
    net.load.loc[:,"p_mw"] = net.load.loc[:,"min_p_mw"] = net.load.loc[:,"max_p_mw"] = res_load.loc[:,"p_mw"]
    net.load.loc[:,"q_mvar"] = net.load.loc[:,"min_q_mvar"] = net.load.loc[:,"max_q_mvar"] = res_load.loc[:,"q_mvar"]
    mult = _bound_less_strict(net.load)
    net.load.loc[:, ["min_p_mw", "max_p_mw", "min_q_mvar","max_q_mvar"]] *= mult #Bound less strict
    
    if consider_ext_grid == True :
        net.ext_grid.loc[:,"min_p_mw"] = net.ext_grid.loc[:,"max_p_mw"] = res_ext_grid.loc[:,"p_mw"]
        net.ext_grid.loc[:,"min_q_mvar"] = net.ext_grid.loc[:,"max_q_mvar"] = res_ext_grid.loc[:,"q_mvar"]
        mult = _bound_less_strict(net.ext_grid)
        net.ext_grid.loc[:, ["min_p_mw", "max_p_mw", "min_q_mvar","max_q_mvar"]] *= mult #Bound less strict
    return net
        
        
def import_max_power(net):
    """Import agent power"""
    data = np.zeros(int(max(net.agent_table["id_network"])+1))
    for k in net.agent_table.index:
        id_network = int(net.agent_table["id_network"][k])
        data[id_network] += 1e-3*net.agent_table.loc[k]["max_p_kw"] # Agregate all agents according to their id_network
    data = np.maximum(data, 0) # Power cannot be produced by load nor consumed by gen.
    return data

def set_max_gen(net):
    """Set all load to 0 and all gen to max power."""
    max_power = import_max_power(net)
    net.load["p_mw"] = 0
    for k in net.gen.index:    
        net.gen.loc[k,"p_mw"] = max_power[k]
    return net

def set_max_load(net):
    """Set all load to max and all gen to 0 power."""
    max_power = import_max_power(net)
    net.gen["p_mw"] = 0
    for k in net.load.index:    
        net.load.loc[k,"p_mw"] = max_power[k]
    return net

def lossen_bounds(net):
    net.bus.loc[:, "min_vm_pu"] *= 0
    net.bus.loc[:, "max_vm_pu"] *= 10
    net.line.loc[:,"max_loading_percent"] *= 10
    return net

############################################################################
############################ Problem with SOCWR ############################
############################################################################

def compute_vm_pu(net):
    """Voltage in res_bus.vm_pu are in reality res_bus.vm_pu**2.
    This function takes the square to get the proper value."""
    net.res_bus.loc[:, "vm_pu"] = np.sqrt(net.res_bus.loc[:, "vm_pu"])

def compute_line_loading(net):
    """Line loadings are wrong when using SOCWR formulation. They are divided by v**2 instead of v.
    The function correct this bug by remultiplying whith v."""
    v_fr = net.res_bus.loc[net.line.from_bus, "vm_pu"].values
    v_to = net.res_bus.loc[net.line.to_bus, "vm_pu"].values
    v = 0.5*(v_fr + v_to) # Lines loading are obtained by multiplying with v. But their is v_from et v_to. Since we cannot know wich one was used, we take the mean.
    net.res_line.loc[:,"loading_percent"] = net.res_line.loc[:,"loading_percent"] * v

############################################################################
################################# COMPUTE ##################################
############################################################################

def get_pmw_from_LMP(LMP, net, id_dual):
    LMP_df = pd.DataFrame({"bus":id_dual["active power balance"]["id_bus"], "LMP":LMP}) # LMP on each bus
    LMP_load = net.load.join(LMP_df.set_index("bus"), on="bus")["LMP"] # LMP associated whith each load
    LMP_gen = net.gen.join(LMP_df.set_index("bus"), on="bus")["LMP"] # LMP associated whith each gen
    LMP_agent = pd.concat([LMP_load, LMP_gen]).sort_index() # LMP associated whith each agent
    poly_cost = deepcopy(net.poly_cost)
    poly_cost.set_index("element", drop=False, inplace=True)
    poly_cost.loc[:,"LMP"] = LMP_agent # Set the cost for all agent
    poly_cost.loc[:,"power"] = (-poly_cost["LMP"] - np.abs(poly_cost["cp1_eur_per_mw"]))/(2*poly_cost["cp2_eur_per_mw2"]+1e-10) # avoid 0 division
    poly_cost.fillna(0)
    res_load = np.maximum(np.minimum(poly_cost.loc[net.load.index, "power"],net.load.loc[net.load.index, "max_p_mw"]),
                          net.load.loc[net.load.index, "min_p_mw"])
    res_load = _convert_res_to_set_power(res_load)
    res_gen = np.maximum(np.minimum(poly_cost.loc[net.gen.index, "power"],net.gen.loc[net.gen.index,"max_p_mw"]),
                         net.gen.loc[net.gen.index,"min_p_mw"])
    res_gen = _convert_res_to_set_power(res_gen)
    return res_load, res_gen

def _convert_res_to_set_power(res):
    """res_load and res_gen should be transformed to DataFrame with 'p_mw' and 'q_mvar' entries."""
    res = res.to_frame()
    res = res.rename(columns = {0:'p_mw'})
    res["q_mvar"] = 0
    return res

def compute_price_per_agent(net):
    """Compute cost associated with each power exchage.
    It does not take into account bounds."""
    p_load = net.res_load.loc[:, "p_mw"]
    p_gen = net.res_gen.loc[:, "p_mw"]
    c0 = net.poly_cost.loc[:, "cp0_eur"]
    c1 = net.poly_cost.loc[:, "cp1_eur_per_mw"]
    c2 = net.poly_cost.loc[:, "cp2_eur_per_mw2"]
    cp_load = c1 + c2*p_load # Price in €/MW
    cp_gen = c1 + c2*p_gen # Price in €/MW
    return cp_load, cp_gen