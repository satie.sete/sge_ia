# -*- coding: utf-8 -*-
"""
Created on Thu Mar 16 15:17:31 2023

@author: Guenole
"""
import numpy as np
# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence

def _init_dic():
    dic = {}
    ##################### Used to create the test case #####################
    # Test case
    dic["rs"] = RandomState(MT19937(SeedSequence(1)))
    dic["network"] = "Eu" # type of network : CIGRE, random_HTA, random_BT, LMP, Eu, Case33, Simplest
    dic["n_bus"] = 200
    dic["n_subnetwork"] = 1
    dic["line_lenght"] = 0.03
    dic["line_lenght_variance"] = 0
    dic["probability"] = "normal" # type of probability for network line_length : normal, weibull, exp
    # Power
    dic["apt_p_kw"] = 1000
    dic["ev_percent"] = 20
    dic["pv_percent"] = 80
    
    dic["scale_load"] = 1
    dic["scale_gen"] = 1
    dic["global_scale"] = 1
    
    dic["allow_q"] = False
    dic["n_agents"] = {"n_apt":40, "n_ev":20, "n_pv":40} # Proportion of each type of agent on the network. They are aggregated agent of the same type. They cannot be located on the same bus.
    
    ##################### Used in the generate data #####################
    dic["plot_type"] = "reduction_dim"
    dic["days"] = None
    dic["reduction_dim"] = 5
    dic["reduction_dim_list"] = [1,3,5,10,20,40,80, 129] # [1,5,10,40,129], [1,5,10,20,40,80, 110, 129] , [1,2,3,4,5,6,8,10,15,20,40,80,110, 129]
    
    dic["model_type"] = "ae" # pca, ae
    dic["all_days"] = np.arange(0,365,1) # (0,365,1)
    dic["time_array"] = np.arange(0, 24*60, 30) # (0, 24*60, 30)
    dic["n_jobs"] = -4
    
    ##################### Used in dimension reduction (fit) #####################
    dic["fit_pca"] = True
    dic["fit_ae"] = False
    dic["fit_ae_influence_LMP"] = False
    dic["n_epochs"] = 500000 # 25000 # 500000
    dic["batch_size"] = 64
    dic["n_hidden"] = 1
    dic["test_ae_training"] = True
    dic["learning_rate"] = 5e-5
    dic["beta_1"] = 0.9
    dic["beta_2"] = 0.999
    # dic[""] = 
    
    ##################### Computed here to be share with different simulations #####################
    _refresh(dic)
    return dic

def _refresh(dic):
    train_days = dic["rs"].choice(dic["all_days"], int(0.6*len(dic["all_days"])), replace=False)
    remain = dic["all_days"][~np.isin(dic["all_days"], train_days)]
    test_days = dic["rs"].choice(remain, int(0.2*len(dic["all_days"])), replace=False)
    val_days = remain[~np.isin(remain, test_days)]
    
    dic["train_days"] = train_days
    dic["test_days"] = test_days
    dic["val_days"] = val_days
    
    dic["folder_struc"] = {dic["plot_type"] : dic[dic["plot_type"]+"_list"],
                           "folder" : []}
    
    if dic["fit_ae_influence_LMP"] == True :
        print("Warning : dic['reduction_dim_list'] as been change because of the use of dic['fit_ae_influence_LMP']=True.")
        dic["reduction_dim_list"] = np.arange(0,131,1) # Number of duals (each should be tested)
        
def _n_job(dic, device="Bertha"):
    if device == "Bertha":
        dic["n_jobs"] = -4
    elif device == "Roman":
        dic["n_jobs"] = 15
    elif device == "Gueno":
        dic["n_jobs"] = 10

def _test(dic, test=False):
    if test :
        dic["reduction_dim_list"] = [1,5,10,40,129]
        dic["all_days"] = np.arange(0,365,5)
        dic["time_array"] = np.arange(0, 24*60, 2*60)
        
def init_random_network(test = False):
    dic = _init_dic()
    dic["fit_pca"] = True
    dic["fit_ae"] = False
    dic["fit_ae_influence_LMP"] = False
    dic["network"] = "random_BT"
    dic["model_type"] = "ae"
    

######################################################################################################################################
############################################################### SGE RL ###############################################################
######################################################################################################################################
    
def _init_dic_RL():
    dic = {}
    
    dic["n_agents"] = {"apt":40, "ev":10, "pv":30}
    dic["epex_year"] = 2021
    dic["line_capacity"] = 50
    dic["price_DSO_max"] = 0.4
    dic["alpha_exchange"] = 0.5
    dic["alpha_congestion"] = 0.5
    dic["delta_sell_buy"] = 1e-5
    dic["delta_convex"] = 1e-9
    dic["mean_price"] = {"apt":0.15, "ev":0.15, "pv":0.05}
    dic["std_price"] = {"apt":0.05, "ev":0.05, "pv":0.05/3}
    dic["non_flex_price"] = 2
    dic["non_flex_power"] = 0.9
    dic["flex_power"] = 0.6

    dic["random_seed"] = False
    
    dic["number_of_time_step"] = 24*60 # 1 minute time step
    dic["t_init"] = 0 # np.random.randint(0,60*24)
    
    dic["number_of_days"] = 365
    dic["all_days"] = np.arange(0,dic["number_of_days"],1)
    dic["mode"] = "train" # "test" "train"
    dic["train_size"] = 0.968
    
    # For the simulation
    dic["n_training_envs"] = 12
    dic["n_episode"] = 12
    _refresh_RL(dic)
    return dic
    
def _refresh_RL(dic):
    if dic["random_seed"] :
        dic["rd_state"] = np.random.randint(1e6)
        dic["seed"] = RandomState(MT19937(SeedSequence(dic["rd_state"])))
    else :
        dic["rd_state"] = 488281
        dic["seed"] = RandomState(MT19937(SeedSequence(dic["rd_state"])))
    dic["rd_state_day"] = dic["rd_state"]
    dic["seed_day"] = RandomState(MT19937(SeedSequence(dic["rd_state_day"])))
    rs = dic["seed"]
    dic["train_days"] = rs.choice(dic["all_days"], int(dic["train_size"]*len(dic["all_days"])), replace=False)
    dic["test_days"] = dic["all_days"][~np.isin(dic["all_days"], dic["train_days"])]
    
def _set_new_seed(dic, seed):
    dic["rd_state"] = seed
    dic["random_seed"] = False
    _refresh_RL(dic)
    
    
    
    
    
    
    
    
    
    
    